package com.rsb.belajarkotlin.RoadMap2_KotlinOOP.oop34_singleton_object

object Utilities {
    val nameAPP = "Belajar Kotlin"
    val versionAPP = 1.0
    fun printNameAPP(name: String) : String {
        return name.toUpperCase()
    }
}