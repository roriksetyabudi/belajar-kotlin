package com.rsb.belajarkotlin.RoadMap2_KotlinOOP.oop35_companion_object

class Application (val name: String) {
    companion object {
        fun hay(name: String) : String {
            return name
        }
    }
}