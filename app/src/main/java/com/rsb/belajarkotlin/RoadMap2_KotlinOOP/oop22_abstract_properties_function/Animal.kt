package com.rsb.belajarkotlin.RoadMap2_KotlinOOP.oop22_abstract_properties_function

abstract class Animal {
    abstract val name: String
    abstract fun run()
}
class Cat: Animal(){
    override val name:String = "Cat"
    override fun run() {
        println("Car RUN!")
    }
}
class Dog: Animal(){
    override val name: String = "dog"
    override fun run() {
        println("Dog RUN!")
    }
}