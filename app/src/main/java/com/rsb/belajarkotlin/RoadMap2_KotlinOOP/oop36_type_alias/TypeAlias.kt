package com.rsb.belajarkotlin.RoadMap2_KotlinOOP.oop36_type_alias

typealias App = Application

typealias StringSupplier = () -> String
fun sayHello(supplier: StringSupplier){
    println("Hallo Supplier ${supplier()}")
}
fun main() {
    val app = App("RORIK")
    println(app.hay("SETYA BUDI"))
    println(sayHello { "Supplier Rorik" })
}