package com.rsb.belajarkotlin.RoadMap2_KotlinOOP.oop11_inheritance

open class Karyawan (val name: String) {
    fun hay(name: String) {
        println("Hay $name, Nama Saya Adalah ${this.name}")
    }
}
class Manager(name: String) : Karyawan(name)
class VicePresident(name: String) : Karyawan(name)