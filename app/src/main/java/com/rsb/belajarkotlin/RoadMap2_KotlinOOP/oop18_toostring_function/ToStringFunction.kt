package com.rsb.belajarkotlin.RoadMap2_KotlinOOP.oop18_toostring_function

fun main() {
    val car1 = Car("Avanza")
    val car2 = Car("Inova")
    println(car1)
    println(car2)
}