package com.rsb.belajarkotlin.RoadMap2_KotlinOOP.oop13_PropertiesOverriding

open class Shape {
    open val corner: Int = -1
}
class Rectangle : Shape() {
    override val corner: Int =  4
}
class Circle: Shape(){
    override var corner: Int = 10
}