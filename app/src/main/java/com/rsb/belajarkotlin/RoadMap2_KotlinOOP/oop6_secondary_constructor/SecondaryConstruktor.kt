package com.rsb.belajarkotlin.RoadMap2_KotlinOOP.oop6_secondary_constructor

fun main() {
    println("-------- SECONDARY CONSTRUCTOR --------")
    var fullName = Person("RORIK", "SETYA", "BUDI")
    var firstNameLastName = Person("BUDI", "RORIKS")
    var fullSecondary = Person("RORIK")

    println(fullName.midleName)
    println(fullSecondary.firstName)
    println(firstNameLastName.lastName)

    println("-------- TANPA PRIMARY CONSTRUCTOR --------")
    val firstNameMidleNameNoPrimary = NoPrimaryConstructor("RORIK","SETYA")
    val fullNameNoPrimary = NoPrimaryConstructor("RORIK", "SETYA", "BUDI")
    println("Halo ${firstNameMidleNameNoPrimary.firstName} ${firstNameMidleNameNoPrimary.midleName} ")
    println("Hallo ${fullNameNoPrimary.firstName} ${fullNameNoPrimary.midleName} ${fullName.lastName}")
}