package com.rsb.belajarkotlin.RoadMap2_KotlinOOP.oop23_getter_setter

import kotlin.reflect.KProperty

fun main() {
    val bignote = BigNote("belajar kotlin")
    println(bignote.bigNote)
}