package com.rsb.belajarkotlin.RoadMap2_KotlinOOP.oop45_annotation
@Fancy(author = "RORIK")
class MyApplication (val name: String, val version: Int) {
    fun info(): String = "Application $name-$version"
}