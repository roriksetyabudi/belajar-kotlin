package com.rsb.belajarkotlin.RoadMap2_KotlinOOP.oop37_inline_class

inline class Token(val value: String) {
    fun screetKey() : String = value
    fun publicKey() : String = value
}