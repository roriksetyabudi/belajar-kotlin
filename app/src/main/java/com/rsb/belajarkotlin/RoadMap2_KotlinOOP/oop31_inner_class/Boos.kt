package com.rsb.belajarkotlin.RoadMap2_KotlinOOP.oop31_inner_class

class Boos(val boosNname: String) {
    inner class Karyawan(val name: String) {
        fun hay(){
            println("Hay $name, dan Atasan Saya Adalah ${this@Boos.boosNname}")
        }
    }
}