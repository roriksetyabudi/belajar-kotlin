package com.rsb.belajarkotlin.RoadMap2_KotlinOOP.oop16_any_class

fun main() {
    val smartphone = Smartphone("Samsung Note 10", "Android")
    println(smartphone.toString())
    println(smartphone.hashCode())

}