package com.rsb.belajarkotlin.RoadMap2_KotlinOOP.oop45_annotation

@Target(AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
@MustBeDocumented
annotation class Fancy(val author: String )
