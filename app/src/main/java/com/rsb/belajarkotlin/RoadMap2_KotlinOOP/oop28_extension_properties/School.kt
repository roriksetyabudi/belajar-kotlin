package com.rsb.belajarkotlin.RoadMap2_KotlinOOP.oop28_extension_properties

class School (val name: String, val adrees: String)

val School.upperName : String
    get() = this.name.toUpperCase()
val School.UpperAddress: String
    get() = this.adrees.toUpperCase()