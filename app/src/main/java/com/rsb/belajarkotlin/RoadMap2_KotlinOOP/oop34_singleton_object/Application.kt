package com.rsb.belajarkotlin.RoadMap2_KotlinOOP.oop34_singleton_object

class Application(val name: String) {
    object Utilites {
        fun hay(name: String) : Unit {
            println("Hello $name")
        }
    }
}