package com.rsb.belajarkotlin.RoadMap2_KotlinOOP.oop39_lazy_properties

class Account {
    val name: String by lazy {
        println("Name Is call")
        "RORIK"
    }
}