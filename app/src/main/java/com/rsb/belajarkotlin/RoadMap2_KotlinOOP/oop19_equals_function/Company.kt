package com.rsb.belajarkotlin.RoadMap2_KotlinOOP.oop19_equals_function

class Company(val name: String) {
    override fun equals(other: Any?) : Boolean {
        return when(other) {
            is Company -> other.name == this.name
            else -> false
        }
    }
}