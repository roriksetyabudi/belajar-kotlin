package com.rsb.belajarkotlin.RoadMap2_KotlinOOP.oop49_polymorphism

import com.rsb.belajarkotlin.RoadMap2_KotlinOOP.oop11_inheritance.Karyawan
import com.rsb.belajarkotlin.RoadMap2_KotlinOOP.oop11_inheritance.Manager
import com.rsb.belajarkotlin.RoadMap2_KotlinOOP.oop11_inheritance.VicePresident

fun main() {
    var karyawan = Karyawan("RORIK")
    karyawan.hay("BUDI")

    karyawan = Manager("SETYA")
    karyawan.hay("NADIA")

    karyawan = VicePresident("NADELA")
    karyawan.hay("SETYA")
}