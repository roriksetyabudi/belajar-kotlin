package com.rsb.belajarkotlin.RoadMap2_KotlinOOP.oop19_equals_function

fun main() {
    val company1 = Company("RSUD PACITAN")
    val company2 = Company("RSUD PACITAN")
    println(company1 == company2)
    println(company1 == company1)

}