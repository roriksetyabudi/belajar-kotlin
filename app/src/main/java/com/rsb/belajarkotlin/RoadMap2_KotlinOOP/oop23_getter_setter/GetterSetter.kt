package com.rsb.belajarkotlin.RoadMap2_KotlinOOP.oop23_getter_setter

fun main() {
    var note = Note("Belajar Kotlin")
    note.title = "Belajar Setter Getter"
    println(note.title)
}