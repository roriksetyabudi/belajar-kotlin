package com.rsb.belajarkotlin.RoadMap2_KotlinOOP.oop32_anonymous_class

fun warAction(action: Action) {
    action.action()
}

fun main() {
    warAction(object: Action {
        override fun action() {
            println("Action One")
        }
    })
    warAction(object: Action {
        override fun action() {
            println("Action Two")
        }
    })
    warAction(object: Action {
        override fun action() {
            println("Action Three")
        }
    })
}