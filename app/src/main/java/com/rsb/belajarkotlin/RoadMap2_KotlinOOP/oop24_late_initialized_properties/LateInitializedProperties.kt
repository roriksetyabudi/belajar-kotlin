package com.rsb.belajarkotlin.RoadMap2_KotlinOOP.oop24_late_initialized_properties

fun main() {
    var television = Television()
    television.initTelevision("POLITRON")
    println(television.brand)
    television.initTV("SAMSUNG")
    println(television.brand)
}