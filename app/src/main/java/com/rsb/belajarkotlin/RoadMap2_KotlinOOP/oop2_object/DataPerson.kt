package com.rsb.belajarkotlin.RoadMap2_KotlinOOP

import com.rsb.belajarkotlin.RoadMap2_KotlinOOP.oop2_object.Person


fun main() {

    val rorik = Person()
    rorik.firsName = "RORIK"
    val setya = Person()
    setya.midleName = "SETYA"
    val budi = Person()
    budi.lastName = "BUDI"

    println(rorik.firsName)
    println(setya.midleName)
    println(budi.lastName)
}