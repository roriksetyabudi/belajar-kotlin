package com.rsb.belajarkotlin.RoadMap2_KotlinOOP.oop36_type_alias

class Application(val name: String) {

    fun hay(name: String) {
        println("Hay : $name, Nama saya adalah ${this.name}")
    }
}