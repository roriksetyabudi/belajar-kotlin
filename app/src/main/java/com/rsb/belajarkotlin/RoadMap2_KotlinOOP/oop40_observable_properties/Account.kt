package com.rsb.belajarkotlin.RoadMap2_KotlinOOP.oop40_observable_properties

import java.util.*
import kotlin.properties.Delegates

class Account(deskripsi: String = "") {
    val name: String by lazy {
        println("Name Is Call")
        "RORIK"
    }
    var deskripsi: String by Delegates.observable(deskripsi) {
        property, oldValue, newValue ->
            println("${property.name} changes from $oldValue to $newValue")
    }
}