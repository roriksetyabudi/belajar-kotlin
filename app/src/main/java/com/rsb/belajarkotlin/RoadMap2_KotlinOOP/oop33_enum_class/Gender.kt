package com.rsb.belajarkotlin.RoadMap2_KotlinOOP.oop33_enum_class

enum class Gender {
    MALE, FEMALE
}

enum class JenisKelamin(val deskripsi: String) {
    L("Laki-Laki"),
    P("Perempuan");

    fun printDeskripsi(deskripsi: String) {
        println(deskripsi)
    }
}