package com.rsb.belajarkotlin.RoadMap2_KotlinOOP.oop46_annotation_target

@Target(AnnotationTarget.VALUE_PARAMETER,
    AnnotationTarget.PROPERTY_GETTER,
    AnnotationTarget.FIELD)

@Retention(AnnotationRetention.RUNTIME)
@MustBeDocumented
annotation class Beta()

class ExempleTarget(

        @field:Beta val firsname: String,
        @get:Beta val midleName: String,
        @param:Beta val lastName: String)