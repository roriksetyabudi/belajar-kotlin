package com.rsb.belajarkotlin.RoadMap2_KotlinOOP.oop24_late_initialized_properties

class Television {
    lateinit var brand: String
    fun initTelevision(brand: String) {
        this.brand = brand
    }
    fun initTV(nama: String) {
        this.brand = nama
    }
}