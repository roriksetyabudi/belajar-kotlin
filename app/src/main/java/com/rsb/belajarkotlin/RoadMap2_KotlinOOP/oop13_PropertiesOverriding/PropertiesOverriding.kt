package com.rsb.belajarkotlin.RoadMap2_KotlinOOP.oop13_PropertiesOverriding

fun main() {
    val shape = Shape()
    println(shape.corner)

    var rectangle = Rectangle()
    println(rectangle.corner)

    var circle = Circle()
    println(circle.corner)
}