package com.rsb.belajarkotlin.RoadMap2_KotlinOOP.oop6_secondary_constructor

class NoPrimaryConstructor {
    var firstName = ""
    var midleName = ""
    var lastName = ""
    constructor(firstNameParams: String, midleNameParams: String) {
        firstName = firstNameParams
        midleName = midleNameParams
        println("Tanpa Primary Construktor 1")
    }
    constructor(firstNameParams: String, midleNameParams: String, lastNameParams: String) {
        firstName = firstNameParams
        midleName = midleNameParams
        lastName = lastNameParams
        println("Tanpa Primary Constructor 2")
    }
}