package com.rsb.belajarkotlin.RoadMap2_KotlinOOP.oop39_lazy_properties

fun main() {
    val account = Account()
    println(account.name)
    println(account.name)
    println(account.name)
    println(account.name)
}