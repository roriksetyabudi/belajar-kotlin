package com.rsb.belajarkotlin.RoadMap2_KotlinOOP.oop7_properties_constructor

fun main() {
    var data1 = Person("RORIK", "SETYA", "BUDI")
    data1.midleName = "JHON"
    println(data1.firstName)
    println(data1.midleName)
    println(data1.lastName)
}