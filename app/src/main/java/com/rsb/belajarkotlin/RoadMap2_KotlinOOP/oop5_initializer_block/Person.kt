package com.rsb.belajarkotlin.RoadMap2_KotlinOOP.oop5_initializer_block

class Person (firsNameParams: String,
                midleNameParams: String,
                lastNameParams: String) {

    var firsName: String = firsNameParams
    var midleName: String = midleNameParams
    var lastName: String = lastNameParams
    init {
        println("Initializer Block  $firsName $midleName $lastName di Buat")
    }
}