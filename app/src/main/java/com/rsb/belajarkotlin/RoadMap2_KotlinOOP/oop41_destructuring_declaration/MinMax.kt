package com.rsb.belajarkotlin.RoadMap2_KotlinOOP.oop41_destructuring_declaration

data class MinMax(val min: Int, val max: Int)