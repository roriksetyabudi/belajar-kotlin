package com.rsb.belajarkotlin.RoadMap2_KotlinOOP.oop8_Function

fun main() {
    val rorik = Pasien()
    rorik.firstName = "RORIK"
    rorik.midleName = "SETYA"
    rorik.lastname = "BUDI"

    rorik.hayFirstName("RORIK")
    val fullName = rorik.fullname()
    println(rorik.hayFirstName("RORIK SETYA BUDI"))
    println(rorik.hayMidleName("SETYA"))
    println(fullName)

}