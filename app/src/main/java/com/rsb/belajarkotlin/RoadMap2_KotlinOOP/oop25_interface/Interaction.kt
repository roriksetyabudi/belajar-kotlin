package com.rsb.belajarkotlin.RoadMap2_KotlinOOP.oop25_interface

interface Interaction {
    val name: String
    fun hay(name: String)
}

/**
 * Multiple inheritance dengan interface
 */
interface Go {
    fun go() {
        println("GO")
    }
}

/**
 * Inheritance antar interface
 */
interface To : Go {
    fun to(){
        println("TO")
    }
}

interface MoveA {
    fun move() = println("move A")
}
interface MoveB {
    fun move() = println("move b")
}

class Human(override val name: String): Interaction,To, MoveA, MoveB {
    override fun hay(name: String) {
        println("Hallo $name, nama saya adalah ${this.name}")
    }
    override fun move(){
        super<MoveA>.move()
        super<MoveB>.move()
        println("Move Human")
    }
}