package com.rsb.belajarkotlin.RoadMap2_KotlinOOP.oop18_toostring_function

class Car(val name: String) {
    override fun toString() : String {
        return "Nama Car : $name"
    }
}