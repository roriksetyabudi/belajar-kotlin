package com.rsb.belajarkotlin.RoadMap2_KotlinOOP.oop27_extension_function

class Student (val name: String, val age: Int)

fun Student.hay(name: String) {
    println("Hallo $name, Nama Saya Adalaah ${this.name} Usia ${this.age}")
}
fun Student?.Kelas(name: String) {
    if(this != null) {
        println("Hallo $name, Nama Saya Adalah ${this.name}, Usia ${this.age}")
    }
}