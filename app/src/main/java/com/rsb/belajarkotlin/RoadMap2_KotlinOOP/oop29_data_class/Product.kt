package com.rsb.belajarkotlin.RoadMap2_KotlinOOP.oop29_data_class

data class Product (
    val nama: String,
    val price: Int,
    val kategori: String)