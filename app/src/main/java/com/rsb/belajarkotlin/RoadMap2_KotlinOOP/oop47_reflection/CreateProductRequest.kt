package com.rsb.belajarkotlin.RoadMap2_KotlinOOP.oop47_reflection

import com.rsb.belajarkotlin.RoadMap2_KotlinOOP.oop45_annotation.NotBlank

data class CreateProductRequest (
    @NotBlank val id: String,
    @NotBlank val name: String,
    @NotBlank val price: Long)
data class CreateCategoriRequest (
    @NotBlank val id: String,
    @NotBlank val name: String)