package com.rsb.belajarkotlin.RoadMap2_KotlinOOP.oop12_function_overriding

open class Karyawan (var name: String) {
    open fun hay(name: String) {
        println("Hay $name, Nama Saya adalah ${this.name}")
    }
}

open class Manager(name: String) : Karyawan(name) {
    override fun hay(name: String) {
        println("hay $name, Nama Saya ${this.name} Sebagai Manager")
    }
}

class SuperManager(name: String) : Manager(name) {
    override fun hay(name: String) {
        println("hay $name, Nama Saya ${this.name} Sebagai Super Manager")
    }
}

class VicePresident(name: String) : Karyawan(name) {
    override fun hay(name: String) {
        println("Hay $name, Nama Saya ${this.name} Sebagai Vice President")
    }
}

