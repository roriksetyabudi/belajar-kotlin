package com.rsb.belajarkotlin.RoadMap2_KotlinOOP.oop20_hashcode_function

fun main() {
    val company1 = Company("RORIK")
    val company2 = Company("RORIK")
    println(company1.hashCode())
    println(company2.hashCode())
    println(company1.hashCode() == company2.hashCode())
}