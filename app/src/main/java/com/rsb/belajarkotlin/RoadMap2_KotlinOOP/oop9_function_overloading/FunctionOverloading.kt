package com.rsb.belajarkotlin.RoadMap2_KotlinOOP.oop9_function_overloading

fun main() {
    val name = Pasien()

    name.firstName = "RORIK"
    name.midleName = "SETYA"
    name.lastName = "BUDI"

    var paramsHay1 = name.hay("BUDI", "SETYA")
    println(paramsHay1)
    var paramsHay2 = name.hay("INDRA", "HARIADI", "MUHAMMAD")
    println(paramsHay2)
}