package com.rsb.belajarkotlin.RoadMap2_KotlinOOP.oop4_constructor

fun main() {
    val paramsFullName1 = Person("Rorik", "Setya", "Budi")
    paramsFullName1.firstName = "RORIK"
    val paramsFullName2 = Person("Budi", "Setya", "Rorik")
    val paramsFullName3 = Person("Budi", "gunadi", "Alhasil")
    paramsFullName3.lastName = "GUNADI"

    println(paramsFullName1.firstName)
    println(paramsFullName2.midleName)
    println(paramsFullName3.lastName)



}