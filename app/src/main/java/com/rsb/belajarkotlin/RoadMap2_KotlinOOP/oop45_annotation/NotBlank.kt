package com.rsb.belajarkotlin.RoadMap2_KotlinOOP.oop45_annotation

@Target(AnnotationTarget.PROPERTY)
@Retention(AnnotationRetention.RUNTIME)
@MustBeDocumented
annotation class NotBlank