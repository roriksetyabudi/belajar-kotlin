package com.rsb.belajarkotlin.RoadMap2_KotlinOOP.oop41_destructuring_declaration

data class Game (val name: String, val price: Int)