package com.rsb.belajarkotlin.RoadMap2_KotlinOOP.oop10_ThisKeyword

class Pasien (var name: String) {

    fun hay(name: String) : Unit {
        println("Halo $name , Nama Saya Adalah ${this.name}")
    }
}