package com.rsb.belajarkotlin.RoadMap2_KotlinOOP.oop44_exception

fun hay(name: String) {
    if(name.isBlank()) {
        throw ValidateException("Name Is Blank")
    } else {
        println("Halo $name")
    }

}

fun main() {
    try {
        println(hay("RORIK"))
        println(hay(""))
        println(hay(""))
    } catch (error: ValidateException) {
        println("Pesan Error : ${error.message}")
    } catch (error: Throwable) {
        println("Pesan Error : ${error.message}")
    } finally {
        println("Finally akan selalu di jalankan atau di eksekusi")
        println("Program Selesai")
    }


}