package com.rsb.belajarkotlin.RoadMap2_KotlinOOP.oop8_Function

class Pasien {
    var firstName = ""
    var midleName = ""
    var lastname = ""
    fun hayFirstName(yourName: String) : Unit {
        println("hay $yourName Nama Saya Depan Saya Adalah $firstName")
    }
    fun hayMidleName(yourMildeName : String) : Unit {
        println("hay $yourMildeName Nama Tengah Saya $midleName'")
    }
    fun hayLastName(yourLastName: String) : Unit {
        println("Hay $yourLastName Nama Belakang Saya $lastname")
    }
    fun fullname() : String {
        return "$firstName $midleName $lastname"
    }
}