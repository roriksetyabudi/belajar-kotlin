package com.rsb.belajarkotlin.RoadMap2_KotlinOOP.oop43_null_safety

/**
 * checkin null
 */
fun hay(friend: Friend?) {
    if(friend != null) {
        println("Hallo selamat datang : ${friend.name}")
    }
}

/**
 * Elvis operator = menganti output null menjadi yang kita inginkan
 */
fun hay2(friend: Friend?) {
    val name = friend?.name ?: "Kosong"
    println("Hallo $name")
}

fun main() {

    println(hay(Friend("RORIK")))
    println(hay(null))
    println(hay2(Friend("BUDI")))
    println(hay2(null))

}