package com.rsb.belajarkotlin.RoadMap2_KotlinOOP.oop30_sealed_class

sealed class Operation (val name: String)
class Plus: Operation("Add")
class Minus: Operation("Minus")
class Modulo: Operation("Modulo")