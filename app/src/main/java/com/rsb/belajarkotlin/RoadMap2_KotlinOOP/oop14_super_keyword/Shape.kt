package com.rsb.belajarkotlin.RoadMap2_KotlinOOP.oop14_super_keyword

open class Shape {
    open val corner: Int = -1

    /**
     * Super FUnction
     */
    open fun fullName() {
        println("Full Name Di Shape")
    }

}
class Rectangle : Shape(){
    override val corner: Int = 4
    val parentCorner: Int = super.corner

    override fun fullName() {
        println("Full Name Rectangle")
        super.fullName()
    }

}
class Circle : Shape(){
    override val corner: Int = 10
    val parentCornerCircle: Int = 20
}