package com.rsb.belajarkotlin.RoadMap2_KotlinOOP.oop21_abstract_class

fun main() {
    var city = City("PACITAN")
    var country = Country("JAWA TIMUR")
    println(city.name)
    println(country.name)
}