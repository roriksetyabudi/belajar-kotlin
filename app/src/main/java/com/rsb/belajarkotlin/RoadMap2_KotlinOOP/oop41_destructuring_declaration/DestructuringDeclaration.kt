package com.rsb.belajarkotlin.RoadMap2_KotlinOOP.oop41_destructuring_declaration

import android.util.Log

fun minMax(value1: Int, value2: Int) : MinMax {
    return when {
        value1 > value2 -> MinMax(value2, value2)
        else -> MinMax(value1, value2)
    }
}

typealias LoginCallback = (Login) -> Boolean
fun login(login: Login, callback: LoginCallback) : Boolean {
    return callback(login)
}

fun main() {
    val game = Game("Mobile Lagend", 2_000_000)
    val (name, price) = game
    val (min, max) = minMax(1000, 100)
    println(name)
    println(price)
    println(min)
    println(max)

    val request = Login("rorik", "mantap")
    login(request) { (user, pass) ->
        user == "rorik" && pass == "mantap"

    }
}