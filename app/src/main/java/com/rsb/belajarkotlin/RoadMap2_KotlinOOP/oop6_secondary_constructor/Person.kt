package com.rsb.belajarkotlin.RoadMap2_KotlinOOP.oop6_secondary_constructor

/**
 * Primary constructor
 */
class Person (firstNameParams: String,
              midleNameParams: String,
              lastNameParams: String) {

    constructor(firstNameParams: String, lastNameParams: String) : this (firstNameParams, "", lastNameParams) {
        println("Secondary Constructor 1")
    }
    constructor(firstNameParams: String) : this(firstNameParams, "") {
        println("secondary Constructor 2")
    }

    var firstName = firstNameParams
    var midleName = midleNameParams
    var lastName = lastNameParams


}