package com.rsb.belajarkotlin.RoadMap2_KotlinOOP.oop15_super_constructor

open class Shape(var name: String, var shape: Int, var corner: String) {
    constructor(name: String, shape: Int) : this(name, shape, "")
    constructor(name: String) : this(name, -4)
}

class Rectangle : Shape {
    constructor() : super("rectangle", 10)
    constructor(color: String) : super("rectangle", 4, color)
}
