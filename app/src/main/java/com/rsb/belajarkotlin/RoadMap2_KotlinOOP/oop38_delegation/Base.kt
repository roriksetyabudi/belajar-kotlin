package com.rsb.belajarkotlin.RoadMap2_KotlinOOP.oop38_delegation

interface Base {
    fun hay(name: String)
    fun hello(name: String)
}
class MyBase : Base {
    override fun hay(name: String) {
        println("Hallo $name")
    }
    override fun hello(name: String) {
        println("Halo Halo $name")
    }
}

//delegation manual
class MyBaseDelegate(val base: Base) : Base {
    override fun hay(name: String) {
        base.hay(name)
    }
    override fun hello(name: String) {
        base.hay(name)
    }
}
class BaseDelegate(val base: Base) : Base by base