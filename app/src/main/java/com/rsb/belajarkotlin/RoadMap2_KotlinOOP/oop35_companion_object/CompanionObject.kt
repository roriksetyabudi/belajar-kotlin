package com.rsb.belajarkotlin.RoadMap2_KotlinOOP.oop35_companion_object

fun main() {
    println(Application.hay("Belajar Kotlin"))
    //bisa menambahkan .Companion tapi sebenernya tidak perlu
    println(Application.Companion.hay("RORIK COMPANION"))
}