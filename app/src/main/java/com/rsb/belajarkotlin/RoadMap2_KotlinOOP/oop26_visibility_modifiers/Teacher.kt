package com.rsb.belajarkotlin.RoadMap2_KotlinOOP.oop26_visibility_modifiers

open class Teacher(val nama: String) {
    fun tech(){
        println("Teach")
    }
}
class SuperTeacher(nama: String) : Teacher(nama) {
    fun teach(){
        super.tech()
    }
}