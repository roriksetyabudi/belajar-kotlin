package com.rsb.belajarkotlin.RoadMap2_KotlinOOP.oop20_hashcode_function

class Company (val name: String) {
    override fun hashCode(): Int {
        return name.hashCode()
    }
    override fun equals(other: Any?): Boolean {
        return when(other) {
            is Company -> other.name == this.name
            else -> false
        }
    }
}