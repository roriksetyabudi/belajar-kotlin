package com.rsb.belajarkotlin.RoadMap2_KotlinOOP.oop11_inheritance

fun main() {
    val manager = Manager("RORIK")
    manager.hay("SETYA BUDI")
    val vicePresident = VicePresident("BUDI")
    vicePresident.hay("RORIK")


}