package com.rsb.belajarkotlin.RoadMap2_KotlinOOP.oop33_enum_class

fun main() {
    val male = Gender.MALE
    val female = Gender.FEMALE
    val allGender: Array<Gender> = Gender.values()

    /**
     * To String
     */
    val manFromString = Gender.valueOf("MALE")
    val femaleFromString = Gender.valueOf("FEMALE")
    println(male)
    println(female)
    println(manFromString)
    println(femaleFromString)
    println(allGender.toList())

    /**
     * Properties dan Function di Enum Class
     */
    val l = JenisKelamin.L
    val p = JenisKelamin.P

    l.printDeskripsi("Lanang")
    p.printDeskripsi("Wadon")




}