package com.rsb.belajarkotlin.RoadMap2_KotlinOOP.oop5_initializer_block

fun main() {
    val fullName1 = Person("RORIK", "SETYA", "BUDI")
    fullName1.firsName = "XXXXXXXX"
    val fullName2 = Person("BUDI", "RORIK", "SETYA")
    println(fullName1.firsName)
    println(fullName1.midleName)
    println(fullName1.firsName)

    println(fullName2.firsName)
    println(fullName2.midleName)
    println(fullName2.firsName)



}