package com.rsb.belajarkotlin.RoadMap2_KotlinOOP.oop26_visibility_modifiers

fun main() {
    val teacher = Teacher("RORK SETYA BUDI")
    teacher.nama
    println(teacher.nama)
    println(teacher.tech())
    val superTeacher = SuperTeacher("BUDI")
    println(superTeacher.nama)
    println(superTeacher.teach())
}