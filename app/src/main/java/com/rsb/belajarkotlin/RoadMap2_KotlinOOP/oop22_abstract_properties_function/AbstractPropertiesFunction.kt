package com.rsb.belajarkotlin.RoadMap2_KotlinOOP.oop22_abstract_properties_function

fun main() {
    var cat = Cat()
    println(cat.name)
    println(cat.run())
    val dog = Dog()
    println(dog.name)
    println(dog.run())
}