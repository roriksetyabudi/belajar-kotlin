package com.rsb.belajarkotlin.RoadMap2_KotlinOOP.oop29_data_class

fun main() {
    val product = Product("Indomie", 3000, "food")
    val product2 = product.copy(nama = "Sedap", price = 2_500)
    println(product)
    println(product2)
}