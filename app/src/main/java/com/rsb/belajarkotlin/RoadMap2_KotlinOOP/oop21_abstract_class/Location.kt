package com.rsb.belajarkotlin.RoadMap2_KotlinOOP.oop21_abstract_class

abstract class Location (val nama: String)
class City(val name: String) : Location(name)

class Country(val name: String) : Location(name)