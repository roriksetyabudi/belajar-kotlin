package com.rsb.belajarkotlin.RoadMap2_KotlinOOP.oop25_interface

fun main() {
    val human = Human("RORIK")
    println(human.hay("BUDI"))
    human.go()
    human.to()
    human.move()
}