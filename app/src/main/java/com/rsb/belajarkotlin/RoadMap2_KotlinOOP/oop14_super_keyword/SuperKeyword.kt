package com.rsb.belajarkotlin.RoadMap2_KotlinOOP.oop14_super_keyword

fun main() {
    val rectangle = Rectangle()
    println("corner ${rectangle.corner}")
    println("parent corner ${rectangle.parentCorner}")

    /**
     * Super FUnction
     */
    println(rectangle.fullName())

    val circle = Circle()
    println("Corner Circle ${circle.corner}")
    println("Parent Corner Circle ${circle.parentCornerCircle}")
}