package com.rsb.belajarkotlin.RoadMap2_KotlinOOP.oop12_function_overriding

fun main() {
    var manager = Manager("RORIK")
    var vicePresident = VicePresident("SETYA")
    manager.hay("BUDI")
    vicePresident.hay("JOKO")
    var superManager = SuperManager("PURNOMO")
    superManager.hay("UTOMO")
}