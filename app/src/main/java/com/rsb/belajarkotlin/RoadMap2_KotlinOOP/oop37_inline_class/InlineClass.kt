package com.rsb.belajarkotlin.RoadMap2_KotlinOOP.oop37_inline_class

fun main() {
    val screet = Token("123456")
    println(screet.screetKey())
    val public = Token("xxxxxxxxx")
    println(public.publicKey())
}