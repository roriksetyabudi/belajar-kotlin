package com.rsb.belajarkotlin.RoadMap2_KotlinOOP.oop31_inner_class

fun main() {
    val boos = Boos("RORIK SETYA BUDI")
    val rorik = boos.Karyawan("ANDRI")
    val boos1 = Boos("RINO")
    val budi = boos1.Karyawan("KATAM")

    rorik.hay()
    budi.hay()

}