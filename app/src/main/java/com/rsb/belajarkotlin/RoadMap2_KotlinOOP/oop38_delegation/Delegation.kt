package com.rsb.belajarkotlin.RoadMap2_KotlinOOP.oop38_delegation

fun main() {
    val base = MyBase()
    println(base.hay("RORIK"))

    //manual delegate
    val basedelegate = MyBaseDelegate(base)
    println(basedelegate.hay("JOKO"))

    //otomatis
    val baseDelegate = BaseDelegate(base)
    println(baseDelegate.hay("RORIK BASE DELEGATE"))
    println(baseDelegate.hello("BUDI"))


}