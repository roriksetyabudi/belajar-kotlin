package com.rsb.belajarkotlin.RoadMap2_KotlinOOP.oop23_getter_setter

class Note (title: String) {
    var title: String = title
    get() {
        println("Call Getter Function")
        return field
    }
    set(value) {
        println("Call Setter Function")
        if(value.isNotBlank()) {
            field = value
        } else {
            println("Invalid Title")
        }
    }
}