package com.rsb.belajarkotlin.RoadMap2_KotlinOOP.oop17_type_check_casts

import com.rsb.belajarkotlin.RoadMap2_KotlinOOP.oop16_any_class.Handphone
import com.rsb.belajarkotlin.RoadMap2_KotlinOOP.oop16_any_class.Laptop

fun printObject(any: Any) {
    println(any)
}

/**
 * Cast Pengecekan Menggunakan If Expression
 */
fun printObjectCheck(any: Any) {
    if(any is Laptop) {
        println("Nama Laptop : ${any.name}")
    } else if(any is Handphone) {
        println("Nama Handphone : ${any.name}")
    } else {
        println(any)
    }
}

/**
 * Cast Pengecekan menggunakan When expression
 */
fun printObjectCheckWhen(any: Any) {
    when(any){
        is Laptop -> {
            println("When Expression Nama Laptop : ${any.name}")
        }
        is Handphone -> {
            println("When Exression Nama Handphone : ${any.name}")
        }
        else -> {
            println(any)
        }
    }
}

/**
 * Unsafe casts
 */
fun printObjectUnsafeCasts(any: Any) {
    val value = any as String
    println(value)
}

/**
 * Safe Nullable Casts
 */
fun printObjectSafeNullableCasts(any: Any) {
    var value = any as? String
    println(value)
}

fun main() {
    println("-----------------------------")
    printObject("RORIK")
    printObject(29)
    printObject("SETYA")
    printObject("BUDI")
    println("--------- PENGECEKAN IS dan !S ---------")
    printObjectCheck(Laptop("Apple"))
    printObjectCheck(Handphone("Samsung"))
    println("--------- PENGECEKAN WHEN EXPRESSION ---------")
    printObjectCheckWhen("RORIK")
    printObjectCheckWhen(Laptop("Asus ROG"))
    printObjectCheckWhen(Handphone("Xiaomi"))
    println("--------- UNSAFE CASTS ---------")
    printObjectUnsafeCasts("RORIK STRING") //success
    printObjectUnsafeCasts("1") //Sukses
    //printObjectUnsafeCasts(1) //ClassCastException (error)
    println("--------- SAFE NULLABLE CASTS ---------")
    printObjectSafeNullableCasts("RORIK")
    printObjectSafeNullableCasts(1)
    printObjectSafeNullableCasts(2)
}