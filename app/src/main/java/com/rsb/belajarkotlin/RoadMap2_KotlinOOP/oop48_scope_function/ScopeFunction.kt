package com.rsb.belajarkotlin.RoadMap2_KotlinOOP.oop48_scope_function

import com.rsb.belajarkotlin.RoadMap2_KotlinOOP.oop27_extension_function.Student

fun main() {
    val student = Student("RORIK", 20)
    val result1 = student.let {
        "Name : ${it.name} Usia ${it.age}"
    }
    println(result1)
    val result2 = student.run {
        "Name : ${this.name} usia ${this.age}"
    }
    println(result2)
    val result3 = student.apply {
        "Name : ${this.name} usia ${this.age}"
    }
    println(result3)
    val result4 = student.also {
        "Name : ${it.name} Usia ${it.age}"
    }
    println(result4)
    with(student) {
        println(this.name)
        println(this.age)
        println("Nama ${this.name} usia ${this.age}")
    }
}