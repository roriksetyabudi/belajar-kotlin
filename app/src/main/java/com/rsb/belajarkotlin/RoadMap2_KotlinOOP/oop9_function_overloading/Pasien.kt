package com.rsb.belajarkotlin.RoadMap2_KotlinOOP.oop9_function_overloading

class Pasien {

    var firstName: String = ""
    var midleName: String = ""
    var lastName: String = ""

    fun hay(firstName: String, midleName: String) : Unit {
        println("Halo $firstName, Nama Tengah Saya $midleName")
    }
    fun hay(firstName: String, midleName: String, lastName: String) : Unit {
        println("Hay $firstName Nama Lengkap Saya $firstName $midleName $lastName")
    }

}