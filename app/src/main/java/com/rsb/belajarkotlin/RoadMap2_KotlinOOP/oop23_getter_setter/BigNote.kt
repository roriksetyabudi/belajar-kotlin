package com.rsb.belajarkotlin.RoadMap2_KotlinOOP.oop23_getter_setter

class BigNote(val title: String) {
    val bigNote: String
        get() = title.toUpperCase()
}