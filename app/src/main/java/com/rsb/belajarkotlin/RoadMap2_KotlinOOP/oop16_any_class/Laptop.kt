package com.rsb.belajarkotlin.RoadMap2_KotlinOOP.oop16_any_class

class Laptop (val  name: String) // => by default langsung class ANY

open class Handphone (val name: String) // => by default langsung class ANY

class Smartphone(name: String, os: String) : Handphone(name)