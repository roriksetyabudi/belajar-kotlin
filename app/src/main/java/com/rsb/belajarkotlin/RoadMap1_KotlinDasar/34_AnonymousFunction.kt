package com.rsb.belajarkotlin.RoadMap1_KotlinDasar

/**
 * lambda akan mengangap barus terakhir di blok lambda sebagai hasil kembalian
 * kadang kita butuh membuat lambda yang se flexible function, dimana kita bisa mengembalikan
 * hasil dimanapun
 * untuk hal ini, kita bisa menggunakan anonymous function
 * anonymous function sebenarnya mirip dengan lambda, hanya cara membuatnya saja yang sedikit berbeda,
 * masih menggunakan kata kunci fuc
 */

fun main() {

    fun hai(value: String, transformer: (String) -> String) : String {
        val result = transformer(value)
        return result
    }

    val anonymousFunction = fun (value: String) : String {
        if(value.isBlank()) {
            return "Ups Kosong"
        }
        return value.toUpperCase()
    }
    val result1 = hai("RORIK anonymous function", anonymousFunction)
    val result2 = hai("", anonymousFunction)

    println(result1)
    println(result2)

}