package com.rsb.belajarkotlin.RoadMap1_KotlinDasar

/**
 * perlu di ketahui bahwa function itu bisa mengembalikan data
 * untuk memberitahu bahwa function mengembalikan data, maka kita harus menuliskan tipe data kembalian dari function tersebut
 * jika function tersebut kita deklarasikan dengan tipe data pengembalian,
 * maka  jugawajib di dalam function nya kita harus mengembalikan data
 * untuk mengembalikan data dari function, kita bisa menggunakan kata kunci return, diikuti dengan tipe datanya
 */

fun sum(a: Int, b: Int) : Int {
    val total = a + b
    return total
}
fun bagiTambah(a:Int, b: Int, c:Int): Int {
    val totalBagiTambah = a + b / c
    return totalBagiTambah
}
fun printName(firstName:String, midleName:String, lastName:String) : String {
    val fullName = "$firstName $midleName $lastName"
    return fullName
}

fun main() {

    /**
     * Function return type Int
     */
    val total = sum(10 , 50)
    println("Total Sum : $total")
    val totalBagiTambah = bagiTambah(10, 10, 2)
    println(totalBagiTambah)
    /**
     * Function return type String
     */
    val fullName = printName("RORIK", "SETYA", "BUDI")
    println(fullName)

}