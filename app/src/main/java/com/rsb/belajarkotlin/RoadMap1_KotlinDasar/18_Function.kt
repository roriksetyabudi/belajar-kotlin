package com.rsb.belajarkotlin.RoadMap1_KotlinDasar

/**
 * kita sebelumnya sebenernuya sudah menggukan sebuah function, function yang biasa kita gunakan adalah
 * function main = function yang digunakan untuk menjalankan kode kotlin
 * function adalah sebuah blok kode yang sengaja dibuat dalam program agar bisa digunakan secara berulang
 * cara membuat function di kotlin sangat sederhana, hanya dengan menggunakan kata kunci fun
 * lalu diikuti dengan nama function nya dan blok kode isi function nya
 * setelah membuat function, kita bisa mengeksekusi function tersebut dengan memangilnya
 * menggunakan kata kunci nama function nya tadi
 * pembuatan nama function tidak boleh menggunakan spasi ya
 */
fun helloWorld(){
    println("Hallo RORIK SETYA BUDI")
    println("Kita Sedang Belajar Kotlin Dasar")
    var total = 0
    var ii = 0..10
    for (i in ii) {
        println("Nilai i : $i")
        total++
    }
    println("Total Perulangan Sebanya $total")
}
fun main() {
    helloWorld()

}