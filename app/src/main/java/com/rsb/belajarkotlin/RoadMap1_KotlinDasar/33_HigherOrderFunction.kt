package com.rsb.belajarkotlin.RoadMap1_KotlinDasar

/**
 * Higher-order function adalah function yang menggunakan function sebagai parameter atau
 * mengembalikan function
 * penggunakan higher-order function kadang berguna ketika kita ingin membuat function yang general
 * dan ingin mendapatkan input yang fleksibel berupa lambda, yang bisa di deklarasikan oleh
 * user ketika memanggil function tersebut
 */

fun hai(value:String, transformer: (String)-> String) : String {
    return "Halo ${transformer(value)}"
}
fun nameFirset(value: String, transformer1: (String) -> String) : String {
    return "Selama Datang ${transformer1(value)} Di Belajar Kotlin";
}

fun main() {

    val transformerLowerCase = { value: String -> value.toLowerCase() }
    val transformerUpperCase = { value: String -> value.toUpperCase() }
    val namaTransformerLowerCase = { value3: String -> value3.toUpperCase() }

    println(hai("RORIK setya budi", transformerLowerCase))
    println(hai("BUDI setya rorik", transformerUpperCase))
    println(nameFirset("rorik", namaTransformerLowerCase))

    /**
     * Trailing lambda
     * untuk menggabungkan kode agar lebih mudah untuk di bacanya
     * seperti contoh di bawah ini
     */
    val result1 = hai("Triling Lambda Lower Case") { values: String -> values.toLowerCase() }
    val result2 = hai("Triling Lambda upper case") { values: String -> values.toUpperCase() }
    val result3 = nameFirset("Rorik Test Triling Lambda") { values: String -> values.toUpperCase() }

    println(result1)
    println(result2)
    println(result3)

}