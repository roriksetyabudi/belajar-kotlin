package com.rsb.belajarkotlin.RoadMap1_KotlinDasar

import kotlin.system.exitProcess

/**
 * PERULANGAN
 * salah satu fitur perulangan salah satunya adalah Foor Loops
 * for digunakan untuk melakukan perulangan iterasi dari data iterator
 * data iterasi/iterator contohnya adalah (Array, Range, dan Lain-Lain)
 */
fun main() {

    println("--------- OPERASI FOR DI DALAM ARRAY --------- ")
    /**
     * For Array
     * cara penulisan for array
     * val nama variable = arrayOf(isi arraynya)
     * for(variable bebas IN nama variabel yang akan di looping) {
     *  printtln(nama bariabel bebas tadi)
     * }
     */

    var totalName = 0
    val name: Array<String> = arrayOf("RORIK", "SETYA", "BUDI", "PACITAN", "JAWA TIMUR", "INDONESIA")
    for(names in name) {
        println(names)
        totalName++
    }
    println("Total Perulangan For Di Array Sebanyak = $totalName ")
    println("--------- OPERASI FOR DI DALAM RANGE --------- ")
    /**
     * For range
     * cara penulisan range sma dengan di dalam array
     */
    var totalRange = 0
    val rangeAngka = 0..10
    for(barisAngka in rangeAngka) {
        println(barisAngka)
        totalRange++
    }
    println("--------- MENAMPILKAN BILANGAN GANJIL 1 = 10 --------- ")
    var bilangan1 = 1..10
    for(iGanjil in bilangan1 step 2) {
        println(iGanjil)
    }
    println("--------- MENAMPILKAN BILANGAN GENAP 0 = 10 --------- ")
    var bilangan2 = 0..10
    for (iGenap in bilangan2 step 2) {
        println(iGenap)
    }

    println("--------- OPERASI FOR DI DALAM RANGE DOWN TO --------- ")
    for (value in 100 downTo 1 step 5) {
        println(value)
    }

    println("--------- OPERASI FOR KOMBINASI ARRAY DAN RANGE --------- ")
    var ukuranArray = name.size-1
    for (iName in 0..ukuranArray) {
        println("Index $iName =  ${name.get(iName)}")
    }

    println("Total Perulangan For di Dalam Range = $totalRange")


}