package com.rsb.belajarkotlin.RoadMap1_KotlinDasar

/**
 * closure adalah kemampuan sebuah function, lambda atau anonymous function berinteraksi dengan data data di sekiytarnya
 * dalam scope yang sama
 * harap menggunakan fitur closure ini dengan bijak saat kita membuat aplikasi
 */

fun main() {

    var counter:Int = 0
    val lambdaCounter = {
        println("Incremenet")
        counter++

    }
    val anonymousCounter = fun () {
        println("Anonymous Function")
        counter++
    }
    fun functionCounter(){
        println("Function Increment")
        counter++
    }
    lambdaCounter()
    anonymousCounter()
    functionCounter()
    println(counter)

}