package com.rsb.belajarkotlin.RoadMap1_KotlinDasar

fun main() {
    /**
     * Array adalah tipe data yang berisikan kumpulan data dengan tipe data yang sama
     * tipe data array di kotlin direpresentasikan dengan kata kunci Array
     * Array<Tipe data) = arrayOf/diinisialisasi(value / nilai array sesuai tipe data, dipisah dengan tadan ,(kome) )
     * untuk mengakses array menggunakan index, data yang di pisah dengan ,(koma) di urutkan dari 0, index pertama adalah 0
     * OPERASI ARRAY
     * size = untuk mendapatkan panjang array
     * get(index) = untuk mendapat data array di posisi berapa
     * [index] = untuk mendapat data array di posisi berapa
     * set(index, value) = mengubah data posisi index
     * [index] = value = mengubah data posisi index
     */
    val members: Array<String> = arrayOf("rorik","setya","budi")
    val biayaMember: Array<Int> = arrayOf(100,200,300)
    val saldoMember: Array<Long> = arrayOf(1000000000,2000000000,3000000000)

    println("--------- MENDAPATKAN DATA ARRAY --------- ")
    val membersRorik: String = members[0];
    val membersSetya: String = members[1]
    val membersBudi: String = members[2];

    println("membersRorik   : " + membersRorik)
    println("membersSetya   : " + membersSetya)
    println("membersBudi    : " + membersBudi)

    println("--------- MENGUBAH DATA ARRAY --------- ")
    println("membersRorikSebelumDiubah : " + members[0])
    //set rubah data array
    members[0] = "RORIK"
    val membersRorikSetelahDiubah: String = members[0];
    println("membersRorikSetelahDiubah : " + membersRorikSetelahDiubah)

    println("--------- ARRAY NULLABLE --------- ")
    /**
     * Secara standard : array di kotlin tidak boleh null harus di deklarasikan
     * jika terpaksa silahkan gunakan null
     * cara bikin array bisa null adalah
     */
    val membersNull: Array<String?> = arrayOfNulls(3)
    membersNull[0] = "Rorik"
    membersNull[1] = null
    membersNull[2] = "Budi"

    println("membersNull Ke 1 : " + membersNull[0])
    println("membersNull Ke 2 : " + membersNull[1])
    println("membersNull Ke 3 : " + membersNull[2])

}