package com.rsb.belajarkotlin.RoadMap1_KotlinDasar

/**
 * kadang kita sering membuat function yang sangat sederhana
 * misal function hanya berisikan kode blok sederhana, misalnya hanya berisi satu baris
 * jika kita mengalami hal seperti ini, kita bisa mengubahnua menjadi single expression function
 * single expression function adalah deklarasi function hanya dengan 1 baris kode
 * untuk membuat single expression function, kita hanya butuh tanda = (sama dengan) setelah
 * deklarasi nama function dan tipe data pengembalian function
 */
fun pembagian(a: Int, b: Int) : Int = a+b
fun perkalian(a: Int) : Int = a * 5
fun penambahan(a: Int, b: Int, c: Int): Int = a+b+c
fun pengurangan(a: Int, b: Int) : Int = a-b
fun hai(nama: String) : Unit = println("Hai : $nama")


fun main() {
    println(pembagian(100, 5))
    println(perkalian(10))
    println(penambahan(10,10,10))
    println(pengurangan(10, 2))
    val nama = hai("RORIK SETYA BUDI")
    println(nama)
    println(hai("BUDI SETYA RORIK"))


}