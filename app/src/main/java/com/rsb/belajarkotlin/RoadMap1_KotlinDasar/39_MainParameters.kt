package com.rsb.belajarkotlin.RoadMap1_KotlinDasar

/**
 * kadang-kadang saat menjalankan program, kita butuh input parameter dari luar
 * bisa untuk konfigurasi program, ataupun yang lainya
 * kotlin mendukung parameter untuk main function
 */

fun main(args: Array<String>) {
    for(value in args) {
        println(value)
    }
}