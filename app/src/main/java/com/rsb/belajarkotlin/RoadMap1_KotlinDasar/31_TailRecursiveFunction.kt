package com.rsb.belajarkotlin.RoadMap1_KotlinDasar

/**
 * recursive function adalah salah satu kemampuan bagus di kotlin, namun sayangnya
 * ada keterbatasan dalam penggunaan recursive
 * jika recursive function yang kita buat, saat dijalankan memanggil function dirinya sendiri terlalu
 * dalam, maka bisa di mungkinkan akan terjadi error stack overflow
 *
 * penumpukan atau stack overflow di java tidak bisa di tangani
 * namun di kotlin bisa di tangani, yaitu dengan teil recursive function
 * teil recursive function adalah teknik mengubah recursive function yang kita buat,
 * menjadi looping biasa ketika di jalankan
 * tidak semua recursive function bisa secara otomatis dibuat teil recursive function, ada syarat-syaratnya
 *
 * syarat teil recursive function
 * kita harus menambahkan tailrec di functionya
 * saat memanggil function dirinya sendiri, hanya boleh memanggil function dirinya sendiri, tanpa
 * embel-embel operasi dengan data lain
 */

tailrec fun display(value: Int) {
    println("recursive : $value")
    if(value > 0) {
        display(value - 1)
    }
}
tailrec fun factorialTailWhen(value: Int, total: Int = 1) : Int {
    return when(value) {
        1 -> total
        else -> factorialTailWhen(value - 1, total * value)
    }
}
tailrec fun factorialTailIf(value: Int, total: Int = 1) : Int {
    return if(value == 1) {
        total
    } else {
        factorialTailIf(value-1, total * value)
    }
}


fun main() {
    println("--------- TAILRECURSIVE LOOP --------- ")
    display(100)
    println("--------- TAILRECURSIVE WHEN --------- ")
    println(factorialTailWhen(10))
    println("--------- TAILRECURSIVE IF --------- ")
    println(factorialRecursiveIf(10))

}