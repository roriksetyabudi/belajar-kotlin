package com.rsb.belajarkotlin.RoadMap1_KotlinDasar

/**
 * operasi boolean digunakan untuk membandingkan dua buah boolean atau menggabungkan 2 nilai boolean
 * Operasi boolean
 * && = dan
 * || = atau
 * ! = Not / tidak
 */
fun main() {
    println("--------- OPERASI && --------- ")
    /**
     * Operasi &&
     * Operasi && (dan) akan true, jika nilai di kanan dan kiri sama true
     * jika salah satu nilai false maka hasilnya false
     * true && true = true
     * true && false = false
     * false && true = false
     * false & false = false
     */

    val nilaiUjianAkhir = 85
    val nilaiAbsesn = 75;
    val lulusUjian = nilaiUjianAkhir > 75
    val lulusAbsesnsi = nilaiAbsesn > 70

    val apakahLulus = lulusUjian && lulusAbsesnsi
    println("Apakah Lulus : " + apakahLulus)


    println("--------- OPERASI || --------- ")
    /**
     * Operasi ||
     * operasi || (or/atau) akan selalu true, jika nilai kanan atau kiri bernilai true
     * operasi || (or/atau) akan bernilai false, jika di kanan dan kiri false
     * true || true = true
     * true || false = true
     * false || true = true
     * false || false = false
     */

    val nilaiEkstra = 90
    val nilaiAgama = 95
    val nilaiKepribadian = 85
    val lulusEkstra = nilaiEkstra > 85
    val lulusNilaiAgama = nilaiAgama > 80
    val lulusKepribadian = nilaiKepribadian > 90

    val apakahSayaLulus = lulusEkstra || lulusNilaiAgama || lulusKepribadian

    println("Apakah Saya Lulus : " + apakahSayaLulus)

    println("--------- OPERASI ! --------- ")
    /**
     * Operasi ! (not/tidak)
     * !true = false
     * !false = true
     */
    println("Tidak Lulus    : " + !apakahSayaLulus)
    println("Lulus          : " + apakahSayaLulus)



}