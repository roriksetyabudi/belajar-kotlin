package com.rsb.belajarkotlin.RoadMap1_KotlinDasar

/**
 * Package adalah tempat yang bisa digunakan untuk mengorganisir kode program yang kita buat di kotlin
 * dengan menggunakan package, kita bisa merapikan kode program kotlin yang kita buat
 * penamaan package di kotlin biasanya menggunakan huruf kecil semua
 * jika ingin membuat sub package bisa menggunakan tanda .(titik)
 */

/**
 * contoh package bisa di lihat pada kode kotlin ini
 * yang paling atas
 * package com.rsb.belajarkotlin.RoadMap1_KotlinDasar
 * berarti semua data ada di package RoadMap1_KotlinDasar
 */

/**
 * import
 * secara standard, file kotlin hanya bisa mengakses file kotlin lainya yang berada dalam package yang sama
 * jika kita ingin mengakses file kotlin yang berada di luar package, maka kita bisa menggunakan Import
 * saat melakukan Import, kita bisa memilih ingin meng-import bagian-bagian tertentu atau semua file
 * untuk mengakses isi sema file cukup tambahkan .* di paling belakang kode import
 */

fun main() {

}