package com.rsb.belajarkotlin.RoadMap1_KotlinDasar

/**
 * do while adalah perulangan yang hampir sama dengan While Loop
 * yang membedakan adalah, pada do while loop, kode blok akan dijalankan dulu
 * baru di akhir akan dilakukan pengecekan kondisi
 * Perbedaan do while dan while adalah
 * While = kode akan di cek dulu, baru di jalankan perulangan
 * do While = kode akan di jalanan dulu, baru dijalanlan perulangan
 */
fun main() {
    var i = 10
    do {
        println("Perulangan Do While Ke i $i")
        i++

    } while (i < 10)
    println("Selesai Perulangan Do While")
}