package com.rsb.belajarkotlin.RoadMap1_KotlinDasar

/**
 * pada sebelumnya kita sudah membahas tentang higher-order function yang merupakan satu fitur  yang sangat berguna
 * namun penggunaan higher-order function akan berdampak terhadap performa saat aplikasi berjalan
 * karena harus membuat subject lambda berulang-ulang. jika penggunaanya tidak terlalu banyak
 * mungkin tidak akan terasa, tetapi jika banyak sekali, maka akan terasa impectnya
 * inline function adalah kemampuan di kotlin yang mengubah higher-order function menjadi inline function
 * dimana dengan inline function, code di dalam higher-order function akan di duplikasi agar pada saat runtime,
 * aplikasi tidak perlu membuat oop2_object lambda berulang-ulang
 */

/**
 * NonInline
 * saat menandai bahwa function adalah inline, maka secara otomatis semua parameter akan menjadi inline
 * jika kita ingin memberitahu bahwa jangan melakukan inline terhadap parameter, kita bisa manandai parameter tersebut dengan
 * kata kunci noninline
 */

inline fun hay(value: () -> String ) : String {
    return "Hay ${value()} Selamat Datang belajar kotlin ya"
}
inline fun hayNoInline(firstname: () -> String, noinline lastName: () -> String): String {
    return "Halow ${firstname()} ${lastName()}"
}

fun main() {
    println("--------- INLINE --------- ")
    println(hay { "JOKO INDRAWAN" })
    println(hay { "BUDI GUNADI" })
    for (i in 0..10) {
        println(hay { "RORIK SETYA BUDI" })
    }
    println("--------- NOINLINE --------- ")
    for(ii in 0..100) {
        println(hayNoInline({ "RORIK" }, { "SETYA BUDI" }))
    }



}