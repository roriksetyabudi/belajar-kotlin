package com.rsb.belajarkotlin.RoadMap1_KotlinDasar

/**
 * Dalam kotrlin, if adalah salah sat kunci yang digunakan untuk melakukan percabangan
 * percabangan artinya, bisa mengeksekusi kode program tertentu ketika suatu kondisi terpenui
 * hampir semua bahasa pemrogramman mendukung if expression
 *
 * Cara menggunakan if expression
 * if(kondisi boolean true atau false) {
 *  jika bernilai true, misi blok ( {} ) if akan di eksekusi
 *  jika bernilai false, maka blok tidak akan di eksekusi
 * }
 *
 */

/**
 * Else Expression
 * blok if akan di eksekusi ketika kondisi if bernilai true
 * terkadang kita ingin melakukan eksekusi program tertentu jika kondisi if bernilai false
 * hal ini bisa dilakukan menggunakan else expression
 *
 * cara menggunakan else expression
 * if(kondisi boolean true atau false) {
 *  jika bernilai true, misi blok ( {} ) if akan di eksekusi
 *  jika bernilai false, maka blok tidak akan di eksekusi
 * } else {
 *  jika kondisi bernilai false
 *  maka isi blok else yang akan di eksekusi
 * }
 *
 */

/**
 * Else if expression
 * kadang dalam if, kita butuh membuat beberapa kondisi
 * kasus seperti ini, di kotlin bisa menggunakan else if expression
 *
 *
 * cara menggunakan if else expression
 * if(kondisi boolean true atau false) {
 *  jika bernilai true, misi blok ( {} ) if akan di eksekusi
 * } else if(kondisi boolean ke 2) {
 *  jika kondisi 2 bernilai true
 * } else {
 *  jika kondisi bernilai false
 * }
 */

fun main() {

    val nilaiAkhir = 81
    if(nilaiAkhir > 80) {
        println("Comloude")
    } else if(nilaiAkhir > 70) {
        println("Baik")
    } else if(nilaiAkhir > 60) {
        println("Cukup")
    } else {
        println("Tidak Lulus")
    }

}