package com.rsb.belajarkotlin.RoadMap1_KotlinDasar

/**
 * saat membuat function, kadang-kadang kita membutuhkan data dari luar, atau kita sebut parameter
 * di kotlin, kita bisa menambahkan parameter di function, bisa lebih dari satu
 * parameter tidaklah wajib, jadi kita bisa membuat function tanpa parameter seperti sebelumnya yang sudah kita buat
 * namun jika kita menambahkan parameter di function, maka ketika memanggil function tersebut,
 * kita wajib memasukan data ke parameternya
 * parameter secara default adalah val
 *
 */

fun namaLengkap (firstName: String, centerName: String, lastName: String, fullName: String?) {
    if(fullName == null) {
        println("Hallo $firstName $centerName $lastName")

    } else {
        println("Hallo $fullName")
    }

}
fun main() {
    namaLengkap("RORIK", "SETYA", "BUDI", null)
    namaLengkap("BUDI", "SETYA", "RORIK", "RSB")
}