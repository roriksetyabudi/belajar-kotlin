package com.rsb.belajarkotlin.RoadMap1_KotlinDasar

/**
 * recursive function adalah function yang memanggil function dirinya sendiri
 * kadang dalam pekerjaan, kita sering menemui kasus dimana menggunakan recursive function lebih
 * mudah dibandingkan tidak menggunakan recursive function
 * contoh kasus yang lebih muda diselesaikan menggunakan recursive function adalah faktorial
 */
fun factorialLoop(value: Int) : Int {
    var result = 1
    for(i in value downTo 1) {
        result *= i
    }
    return result
}

fun factorialRecursiveWhen(value: Int) : Int {
    return when(value) {
        1 -> 1
        else -> value * factorialRecursiveWhen(value-1)
    }
}
fun factorialRecursiveIf(value: Int ) : Int {
    return if(value == 1) {
        1
    } else {
        value * factorialRecursiveIf(value - 1)
    }
}

fun main() {

    println("--------- FACTORIAL LOOP --------- ")
    println(factorialLoop(10))
    println("--------- FACTORIAL RECURSIVE WHEN --------- ")
    println(factorialRecursiveWhen(10))
    println("--------- FACTORIAL RECURSIVE IF --------- ")
    println(factorialRecursiveIf(10))
}