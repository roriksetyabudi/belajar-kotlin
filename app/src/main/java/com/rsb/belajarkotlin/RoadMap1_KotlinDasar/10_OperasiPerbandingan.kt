package com.rsb.belajarkotlin.RoadMap1_KotlinDasar

/**
 * Operasi perbandingan adalah operasi untuk membandingkan dua buah data
 * operasi perbandingan adalah operasi yang menghasilkan nilai boolean true dan false (benar atau salah)
 * jika nilai hasilnya benar maka akan menghasilkan true
 * jika nilai hasilnya salah maka akan menghasilkan false
 * Operator perbandingan
 * > = lebih dari
 * < = kurang dari
 * >= lebih dari sama dengan
 * <= kurang dari sama dengan
 * == sama dengan
 * != tidak sama dengan
 * operator perbandingan tidak hanya untuk number saja tetapi tipe data string pun bisa
 */
fun main() {
    val a = 100
    val b = 200

    val result1:Boolean = a > b
    val result2:Boolean = a >= b
    val result3:Boolean = a < b
    val result4:Boolean = a <= b
    val result5:Boolean = a == b
    val result6:Boolean = a != b

    println("a = 100, b = 200")
    println("a lebih dari b                 : " + result1)
    println("a lebih dari sama dengan b     : " + result2)
    println("a kurang dari b                : " + result3)
    println("a kurang dari sama dengan b    : " + result4)
    println("a sama dengan b                : " + result5)
    println("a tidak sama dengan b          : " + result6)
}