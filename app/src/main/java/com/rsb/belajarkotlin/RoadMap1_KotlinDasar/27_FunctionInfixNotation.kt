package com.rsb.belajarkotlin.RoadMap1_KotlinDasar

/**
 * infix notation adalah operasi yang bisa dilakukan di operasi matematika, dimana dia melakukan
 * operasi terhadap dua data
 * hampir semua operasi matematika adalah infix notation
 * di kotlin, kita bisa membuat function infix notation juga
 * untuk menggunakan function infix notation, tidak wajib menggunakan tanda .(titik)
 *
 * syarat function infix notation
 * harus sebagai function member atau function extension
 * harus memiliki 1 parameter
 * parameter tidak boleh varargs dan tiak boleh memiliki nilai default
 */

infix fun String.to(name: String) : String {
    if(name == "LOWERCASE") {
        return this.toLowerCase()
    } else {
        return this.toUpperCase()
    }
}

fun main() {
    var nama = "rorik setya budi" to "UPPER"
    println(nama)
}