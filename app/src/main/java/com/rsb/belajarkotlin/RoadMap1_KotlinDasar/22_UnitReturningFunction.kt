package com.rsb.belajarkotlin.RoadMap1_KotlinDasar

/**
 * Function di kotlin ada dua jenis, pertama tidak mengembalikan nilai, yang kedua mengembalikan nilai
 * Function-function yang sebelumnya sudah kita buat addalah function yang tidak mengembalikan nilai
 * sebenarnya, function-funtion yang sudah kita buat sebelunya, dia mengembalikan tipe data unit.
 * dimana unit adalah tanda bahwa function tersebut tidak mengembalikan apa-apa
 * penulisan unit adalah tidak wajib, namun jika kita menulis tipe data pengembalian sebuah function,
 * maka secara otomatis dia adalah unit
 */

fun nameUnit(nama:String?): Unit {
    if(nama == null) {
        println("Nama Kosong")
    } else {
        println("Halo Nama $nama")
    }
}

fun main() {
    nameUnit("RORIK SETYA BUDI")
}