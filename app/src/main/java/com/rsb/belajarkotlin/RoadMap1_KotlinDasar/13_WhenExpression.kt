package com.rsb.belajarkotlin.RoadMap1_KotlinDasar

/**
 * selain if expression untuk melakukan percabangan di kotlin, kita juga bisa menggunakan When expression
 * When expression sangat sederhana di banding if
 * bisanya when expression digunakan untuk pengecekan ke kondisi dalam satu variable
 */


fun main() {

    val gradeNilai = "A"

    println("--------- OPERASI WHEN --------- ")
    when(gradeNilai) {
        "A" -> {
            println("Sangat Baik")
        }
        "B" -> {
            println("Baik")
        }
        "C" -> {
            println("Cukup")
        }
        "D" -> {
            println("Sangat Tidak Cukup")
        }
        "E" -> println("Tidak Lulus")
        else -> println("UPS Operasi Tidak Tersedia")
    }

    /**
     * when expression multiple option atau multiple check
     */

    when(gradeNilai) {
        "A", "B", "C" -> {
            println("Selamat Anda Lulus")
        }
        else -> println("Anda Tidak Lulus")
    }

    /**
     * When Expression in
     */
    val passingGrane: Array<String> = arrayOf("A", "B", "C")
    when(gradeNilai) {
        in passingGrane -> println("LULUS PASING GRADE")
        !in(passingGrane) -> println("Tidak Lulus PASING GRADE")
    }

    /**
     * When Expression is
     * is digunakan untuk melakukan pengecekan tipe datanya , pada suatu variable bosa di cek tipe datanya
     * menggunakan expression when
     */
    val name = "RORIK SETYA"
    when(name) {
        is String -> println("Name is String")
        !is String -> "name not String"
    }

    /**
     * When sebagai pengganti if else
     * selain pengecekan terhadap variable
     * when juga dapat digunakan sebagau pengganti if else
     * untuk mengganti if else dengan when, kita tidak perlu menggunakan variable dalam when
     */

    val nilaiPasingGrade = 40
    when {
        nilaiPasingGrade > 80 -> {
            println("Lulus Dengan Peringkat A")
        }
        nilaiPasingGrade > 70 -> {
            println("Lulus Dengan Peringkat B")
        }
        nilaiPasingGrade > 60 -> {
            println("Lulus Dengan Peringkat C")
        }
        nilaiPasingGrade > 40 -> {
            println("Lulus Dengan Nilai D")
        }
        else -> println("Tidak Lulus")
    }





}