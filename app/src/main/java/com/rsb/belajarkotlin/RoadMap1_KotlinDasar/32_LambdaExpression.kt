package com.rsb.belajarkotlin.RoadMap1_KotlinDasar

/**
 * function di kotlin adalah first-class citizens, artinya dianggap seperti tipe data yang lainya
 * bisa disimpan di variable, array, bahkan bisa di kirim ke parameter function itu sendiri
 * lambda expression secara sederhana adalah function yang tidak memiliki nama
 * biasanya saat kita membuat function, kita akan selalu membuat menggunakan kata kunci fun dan
 * mendeklarasikan nama functionya
 * dengan lambda expression, kita bisa membuat function tanpa harus mendeskripsi functionya
 */

fun namaLengkap(value: String) : String = value.toUpperCase()

fun main() {

    val lambdaNama: (String, String, String) -> String = { value: String, value2, value3->
        val result = "$value $value2 $value3";
        result
    }
    val lambdaAlamat: (String, String) -> String = {value1: String, value2: String ->
        val result2 = "$value1 $value2"
        result2

    }

    println(lambdaNama("RORIK", "SETYA", "BUDI"))
    println(lambdaAlamat("PACITAN","JAWA TIMUR"))
    println("--------- IT --------- ")
    /**
     * It hanya bisa digunakan kalau parameternya hanya satu
     * jika lebih dari satu maka tidak bisa
     */
    val lambdaNamaIt: (String) -> String = {
        it.toUpperCase()
    }
    println(lambdaNamaIt("rorik setya budi test lambda it"))
    println("--------- METHOD REFERENCE --------- ")
    /**
     * Method reference adalah kita membuat lambda dengan function yang sudah da
     */
    val lambdaNamaLengkap: (String) -> String = ::namaLengkap
    println(lambdaNamaLengkap("NAMA RORIK SETYA BUDI"))


}