package com.rsb.belajarkotlin.RoadMap1_KotlinDasar

/**
 * Operasi Matematika
 * + = penjumlahan
 * - = pengurangan
 * * = perkalian
 * / = pembagian
 * % = sisa pembagian
 * semua operasi bisa dioperasikan pada tipe data number
 * di kotlin operasi matematika sentifik, jadi yang di dahulukan adalah
 * / (pembagian), *(perkalian), + (penjumlahan) - (pengurangan)
 */

fun main() {
    println("--------- OPERASI MATEMATIKA --------- ")
    var resultPembagianInt: Int = 10/2
    var resultPembagianDouble: Double = 10.0 / 3.0
    var resultPembagianSentifik = 10 + 10 / 2
    var resultPerkalian = 10 * 5
    var resultPerkalianSentifik = 10 + 10 * 5
    var resultPengurangan = 10 - 5
    var resultPenguranganSentifik = 10 - 5 * 2
    var resultSisaBagi = 10 % 1

    println("resultPembagianInt         : " + resultPembagianInt)
    println("resultPembagianDouble      : " + resultPembagianDouble)
    println("resultPembagianSentifik    : " + resultPembagianSentifik)
    println("resultPerkalian            : " + resultPerkalian)
    println("resultPerkalianSentifik    : " + resultPerkalianSentifik)
    println("resultPengurangan          : " + resultPengurangan)
    println("resultPenguranganSentifik  : " + resultPenguranganSentifik)
    println("resultSisaBagi              : " + resultSisaBagi)

    println("--------- AUGMENTED ASSIGMENTS PENAMBAHAN --------- ")
    /**
     * untuk mempersingkat operasi matematika
     */
    var total: Int = 0

    val barang1 = 100
    total += barang1
    val barang2 = 200
    total += barang2
    val barang3 = 300
    total += barang3
    val barang4 = 400
    total += barang4
    println("total : " + total)

    println("--------- AUGMENTED ASSIGMENTS PENGURANGAN --------- ")
    var saldo: Int = 1000000

    val buyBarang1 = 100000
    saldo -= buyBarang1
    val buyBaranh2 = 200000
    saldo -= buyBaranh2
    val buyBarang3 = 300000
    saldo -= buyBarang3
    println("Sisa Saldo : " + saldo)

    println("--------- AUGMENTED ASSIGMENTS PERKALIAN --------- ")
    var pendapatan: Int = 10;

    val pendapatan1 = 10
    pendapatan *= pendapatan1
    val pendapatan2 = 20
    pendapatan *= pendapatan2
    val pendapatan3 = 30
    pendapatan *= pendapatan3
    println("Pendapatan : " + pendapatan)

    println("--------- AUGMENTED ASSIGMENTS PEMBAGIAN --------- ")

    var jasa: Double = 100000.0

    val pembagian1 = 10
    jasa /= pembagian1
    val pembagian2 = 5
    jasa /= pembagian2
    val pembagian3 = 20
    jasa /= pembagian3

    println("Jasa : " + jasa)

    println("--------- UNARY OPERATOR --------- ")
    /**
     * Unary Operator
     */
    var nilaiTotal: Int = 100
    nilaiTotal++
    nilaiTotal++
    println("Nilai Total Naik 2         : " + nilaiTotal)
    nilaiTotal--
    nilaiTotal--
    nilaiTotal--
    println("Nilai Total Turun 3        : " + nilaiTotal)
    val suhu: Int = -10
    println("Nilai Total Kurangi 10     : " + suhu)

    var sehat = true
    println("Tidak Sehat                : " + !sehat)


}