package com.rsb.belajarkotlin.RoadMap1_KotlinDasar

/**
 * komentar terbaik pada kode adalah kode itu sendiri
 * saat membuat kode, kita perlu membuat kode semudah mungkin untuk di baca
 * namun kadang juga kita butuh menambahkan komentar di kode kotlin kita
 */

/**
 * ini adalah multi line komentar
 */

//ini adalah satu baris komentar

fun contohFunction1(){

}

/**
 * function contohFunction2
 * @param param nama parameter
 */
fun  contohFunction2(param: String){

}

fun main() {
    //memanggil function contohFunction1
    contohFunction1()
    //memanggil function confohFunction2
    contohFunction2("")

}