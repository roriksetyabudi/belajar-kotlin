package com.rsb.belajarkotlin.RoadMap1_KotlinDasar

/**
 * Break dan continue adalah kata kunci yang bisa digunakan dalam semua perulangan di kotlin
 * break digunakan untuk menghentikan seluruh perulangan
 * continue adalah digunakan untuk menghentikan perulangan yang berjalan,dan langsung melanjutkan
 * ke perulangan selanjutnya
 */

fun main() {
    println("--------- BREAK --------- ")
    /**
     * break
     */
    var i = 0
    while (true) {
        println("urutan Ke : $i")
        i++
        if(i > 10) {
            println("Berhenti DI : $i")
            break
        }
    }

    println("--------- CONTINUE --------- ")
    println("--------- menampilkan bilangan ganjil --------- ")
    /**
     * Continue = digunakan untuk menghentikan perualangan yang sedang berjalan
     * contok kode menampilkan bilangan ganjil
     */
    for (i in 1..100) {

        if(i % 2 == 0) {
            continue
        }
        println("Nilai Ganjil : $i")
    }

}