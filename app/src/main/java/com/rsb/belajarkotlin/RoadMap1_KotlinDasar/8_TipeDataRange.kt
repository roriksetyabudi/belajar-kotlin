package com.rsb.belajarkotlin.RoadMap1_KotlinDasar

/**
 * tipe data range digunakan untuk membuat array yang berisi data angka berurutan
 * misal dari 1 s/d 100, 0 s/d 100, atau digunakan untuk membuat kelipatan
 * cara membuat range di koltin caranya cukup mudah, menggunakan tanda ..(titik dua kali)
 * contohnya
 * 0..10 = range 0 s/d 10
 * 1..100 = range 1 s/d 100
 */

fun main() {

    val range = 1..100 //membuat range 0 s/d 100
    println(range)

    /**
     * Operasi di range
     * count = mendapatkan total data di range
     * contanins(value) = mengecek apakah terdapat value tersebut atau tidak
     * first = mendapatkan nilai pertama
     * last = mendapatkan nilai akhir
     * step = mendapatkan nilai tiap kenaikan
     */

    println("--------- OPERASI RANGE --------- ")
    println("count              : " + range.count())
    println("contanins true     : " + range.contains(50))
    println("contanins false    : " + range.contains(101)) //false karena nilai lebih dari 100
    println("first              : " + range.first)
    println("last               : " + range.last)
    println("step               : " + range.step)

    /**
     * Range Terbail
     * kalau terbalik untuk membuat range tidak perlu mennggunakan ..(titik dua) tetapi menggunakan downTo
     *
     */
    println("--------- RANGE TERBALIK --------- ")
    val rangeDownTO = 100 downTo 1
    println("--------- OPERASI RANGE TERBALIK --------- ")
    println("count              : " + rangeDownTO.count())
    println("contanins true     : " + rangeDownTO.contains(50))
    println("contanins false    : " + rangeDownTO.contains(101)) //false karena nilai lebih dari 100
    println("first              : " + rangeDownTO.first)
    println("last               : " + rangeDownTO.last)
    println("step               : " + rangeDownTO.step)

    /**
     * Range dengan step
     * digunakan untuk menaikan step tidak hanya satu
     * jadi misal mau naik step 5, 2
     * jadi untuk kenaikan tidak hanya 1
     */
    println("--------- RANGE DENGAN STEP --------- ")
    val rangeStep1 = 1..100 step 2

    println("count              : " + rangeStep1.count())
    println("contanins true     : " + rangeStep1.contains(10))
    println("contanins false    : " + rangeStep1.contains(101)) //false karena nilai lebih dari 100
    println("first              : " + rangeStep1.first)
    println("last               : " + rangeStep1.last)
    println("step               : " + rangeStep1.step)

    println("--------- RANGE DENGAN STEP TERBALIK --------- ")
    val rangeStep2 = 100 downTo 1 step 5
    println("count              : " + rangeStep2.count())
    println("contanins true     : " + rangeStep2.contains(50))
    println("contanins false    : " + rangeStep2.contains(101)) //false karena nilai lebih dari 100
    println("first              : " + rangeStep2.first)
    println("last               : " + rangeStep2.last)
    println("step               : " + rangeStep2.step)

}