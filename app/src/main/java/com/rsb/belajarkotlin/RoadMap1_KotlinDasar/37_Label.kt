package com.rsb.belajarkotlin.RoadMap1_KotlinDasar

/**
 * label adalah penanda
 * semua expression di kotlin bisa di tandai dengan label
 * untuk membuat label di kotlin, cukup menggunakan nama label, lalu diikuti dengan karakter @
 */

/**
 * Salah satu kegunaan label adalah untuk melakukan Break, Continue dan return
 * biasanya break, continue dan return hanya bisa menghentikan proses di blok kode tempat mereka berada,
 * namun dengan menggunakan label, kita bisa menentukan ke label mana kode akan berhenti
 */

fun main() {

    println("--------- LABEL --------- ")
    loopA@ for (a in 1..10) {
        loopB@ for (b in 1..10) {
            println("$a x $b = ${a * b}")
        }
    }
    println("--------- BREAK TO LABEL --------- ")
    loopC@ for (c in 1..10) {
        loopD@ for(d in 1..10) {
            println("$c x $d = ${c * d}")
            if(c == 5 ){
                break@loopC
            }
        }
    }
    println("--------- CONTINUE TO LABEL --------- ")
    loopE@ for (e in 1..10) {
        loopF@ for (f in 1..10) {
            if(f == 5) {
                continue@loopE
            }
            println("$e x $f  = ${e * f}")

        }
    }
    println("--------- RETURN TO LABEL --------- ")
    fun hay(value: String, operation: (String) -> Unit) : Unit = operation(value)
    hay("RORIK SEYA BUDI") test@{
        if(it == "") {
            return@test
        } else {
            println("RORIK SETYA BUDI")
        }
    }

}