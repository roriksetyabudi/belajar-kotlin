package com.rsb.belajarkotlin.RoadMap1_KotlinDasar

/**
 * di kotlin, function parameter wajib diisi ketika memanggil function
 * namun kita juga bisa memasukan nilai default parameter, dengan demikian saat memanggil
 * function tersebut, kita tidak wajib memasukan nilai pada parameternya
 * default parameter ini cocok pada jenis parameter yang sekiranya memang tidak wajib untuk diisi
 */

fun fullName(firstName: String, midleName: String = "", lastName:String = "") {
    println("Hallo $firstName $midleName $lastName")
}
fun main() {
    fullName("RORIK")
    fullName("RORIK", "SETYA")
    fullName("RORIK", "SETYA", "BUDI")
}