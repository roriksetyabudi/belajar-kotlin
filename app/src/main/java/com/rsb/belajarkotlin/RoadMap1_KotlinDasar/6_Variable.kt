package com.rsb.belajarkotlin.RoadMap1_KotlinDasar

/**
 * Membuat constant
 */
const val APP_VERSION = "1.0"
const val APP_NAME = "BELAJAR KOTLIN DASAR";

fun main() {
    /**
     * variable = tempat untuk menyimpan suatu data
     * kotlin mendukung 2 jenis variable yaitu Mutable (bisa di ubah), Immutable (tidak bisa di ubah)
     * untuk membuat variable Mutable (bisa di ubah) meggunakan kata kunci var
     * untuk membuat variable Immutable (tidak bisa di ubah) menggunakan kata kunci val
     * mutable (bisa di rubah) maksudnya adalah data yang berada di dalam variable dengan tipe data yang sama
     * di rekomendasi menggunakan variable immutable dari pada mutable (agar data nya konsisten)
     */

    println("--------- VARIABLE MUTABLE --------- ")
    var firsName: String = "RORIK"
    var lastName: String = "BUDI"
    println("lastName Sebelum di Rubah  : " + firsName);

    //mengubah isi variable
    lastName = "SETYA BUDI"

    println("firsName                   : " + firsName);
    println("lastName Setelah Di rubah  : " + lastName)

    println("--------- VARIABLE IMMUTABLE --------- ")
    val firsNameImmutable:String = "RORIK IMMUTABLE"
    val lastNameImmutable: String = "SETYA BUDI IMMUTABLE"

    println("firsNameImmutable : " + firsNameImmutable)
    println("lastNameImmutable : " + lastNameImmutable)

    println("--------- VARIABLE NUULABLE --------- ")
    /**
     * Secara standard, variable di kotlin harus di deklarasikan / di inisialisasikan nilanya tidak boleh null
     * misalkan terpaksa menggunakan null bisa menggunakan ?(tanda tanya) setelah tipe datanya
     * sangat tidak direkomendasikan untuk mengunakan null di variable /  harus di deklarasikan
     *
     */

    var firsNameNull: String? = "RORIK"
    firsNameNull = null

    //error ketika variabel di akses dengan function tertentu
    //println(firsNameNull.length)  //contoh salah

    //yang bener cara aksesnya dikasih tanda ?(tanda tanya) .diikuti functionya
    println("firsNameNull : " + firsNameNull)
    println("firsNameNull Length: " + firsNameNull?.length)

    println("--------- VARIABLE CONSTANT --------- ")
    /**
     * variable constant adalah immutable data, yang bisa di akses untuk keperluan global (bisa di akses dari mana saja)
     * untuk menandai bahwa variable tersebut adalah constant, biasanya menggunakan UPPER_CASE dalam pembuatan variable constantnya
     * constant wajib menggunakan variable val
     * constant selalu di kasih di paling atas function main
     * untuk membuat constant di awali dengan cons val (NAMA VARIABLE)
     */

    println("Selamat DaTang di $APP_NAME pada Versi $APP_VERSION")




}