package com.rsb.belajarkotlin.RoadMap1_KotlinDasar

/**
 * While Loops
 * while adalah perulangan lain yang ada di kotlin
 * while adalah salah satu perulangan yang sangat flexible, dimana kode while akan melakukan pengecekan kondisi,
 * jika kondisi bernilai true, maka akan menjalankan blok while, dan terus
 * diulangi sampai kondisi while bernilai false
 */
fun main() {

    /**
     * Kode While
     */
    var i = 0
    while (i < 10){
        println("Perulangan $i")
        i++
    }
    println("Selesai Perulangan While")
}