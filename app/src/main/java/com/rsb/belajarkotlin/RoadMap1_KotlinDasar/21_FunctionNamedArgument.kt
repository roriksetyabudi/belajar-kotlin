package com.rsb.belajarkotlin.RoadMap1_KotlinDasar

/**
 * Function named argument
 * kadang ada banyak function yang parameternya banyak sekali
 * jal ini sangat menyulitkan saat kita akan memanggil function tersebut, kita harus benar-benar
 * tahu urutan parameter di function tersebut
 * untungnya kotlin memiliki fitur yang namanya named argument
 * named argument adalah fitur dimana kita bisa menyebut nama parameter saar memanggil function,
 * dengan demikian kita tidak wajib tahu posisi tiap parameter di suatu functionya
 */

fun namaLengkapBanget(firsName: String, midleName: String, lastName:String, usia: Int, tinggi: Int){
    println("Nama $firsName $midleName $lastName Usia $usia Tinggi $tinggi")
}

fun main() {
    namaLengkapBanget(
        firsName = "RORIK",
    lastName = "BUDI",
    midleName = "SETYA",
    tinggi = 80,
    usia = 28)
}