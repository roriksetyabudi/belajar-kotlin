package com.rsb.belajarkotlin.RoadMap1_KotlinDasar

fun main() {
    /**
     * tipe data yang mempunyai 2 nilai TRUE dan FALSE
     * TRUE = benar
     * FALSE = salah
     * boolean di representasikan dengan tipe data Boolean
     */

    var benar: Boolean = true
    var salah: Boolean = false

    println("benar : " + benar)
    println("salah : " + salah)
}