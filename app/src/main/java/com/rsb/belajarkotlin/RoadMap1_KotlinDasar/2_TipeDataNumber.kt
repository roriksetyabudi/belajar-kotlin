package com.rsb.belajarkotlin.RoadMap1_KotlinDasar

/**
 * tipe data data number
 * integer = bulat
 * floating point = pecahan
 * Byte, size 8 bit
 * Short 16 bit
 * int 32 bit - yang sering di gunakan (penghitungan standard)
 * long 64 - lebih dari 2 milyar (digunakan untuk save uang, data keuangan)
 * yang membedakan adalah panjang nilainya
 */

fun main() {

    /**
     * Bilangan Bulat Integer
     * untuk membuat long di ujung nilai/value di kasih tanda L (menandakan long)
     */
    var age: Byte = 30
    var height: Int = 185
    val distance: Short = 2000
    var balance: Long = 100000000L

    println("--------- BILANGAN BULAT NUMBER --------- ")
    println("Age        : " + age)
    println("height     : " + height)
    println("distance   : " + distance)
    println("balance    : " + balance)

    println("--------- BILANGAN FLOAT / PECAHAN --------- ")
    /**
     * Floating Point
     * Float 32 bit
     * Double 64 bit (yang biasa digunakan untuk pecahan)
     * Kalau membuat float di ujungnya nilai/value harus di kasih F (menandakan float)
     */

    var sample: Float = 10.20F
    var radius: Double = 2342342343.54535
    println("Sammple    : " + sample)
    println("radius     : " + radius)

    println("--------- LITERALS --------- ")
    /**
     * decimalliteral = desimal biasa
     * hexadecimalliteral = di depan nilai/value harus di kasih kode (0x) diikuti dengan value selanjutnya (image size)
     * binaryliteral = di depan nilai/vaue harus di kasih kode (0b) diikuti dengan value selanjutnya (detail matematika)
     */
    var decimalliteral: Int = 100
    var hexadecimalliteral: Int = 0xFFFFFF
    var binaryliteral = 0b0001;

    println("decimalliteral         : " + decimalliteral)
    println("hexadecimalliteral     : " + hexadecimalliteral)
    println("binaryliteral          : " + binaryliteral)

    println("--------- UNDERSCORE DI ANGKA --------- ")
    /**
     * underscore nilai = digunakan untuk memudahkan di baca nilai angka
     * ketika di compile underscore akan di abaikan / di ignore
     */
    var price: Int = 1_00_000_000
    var saldo: Long = 1_000_000_000
    println("price : " + price)
    println("saldo : " + saldo)

    println("--------- CONVERTION / KONVERSI --------- ")
    /**
     * digunakan untuk melakukan konversi nilai dari tipe data satu ke tipe data lainya
     * ketika mau melakukan konversi tipe data di depan di kasih .to(tipe datanya)
     */
    var price_sales: Int = 1_000_000
    //Conversion
    var byte: Byte = price_sales.toByte() //int to byte
    var short: Short = price_sales.toShort() //int to short
    var int: Int = price_sales.toInt() //int to int
    var long: Long = price_sales.toLong() //int to long
    var float: Float = price_sales.toFloat() //int to float
    var double: Double = price_sales.toDouble() //int to double

    println("byte   : " + byte)
    println("short  : " + short)
    println("int    : " + int)
    println("long   : " + long)
    println("float  : " + float)
    println("double : " + double)

}