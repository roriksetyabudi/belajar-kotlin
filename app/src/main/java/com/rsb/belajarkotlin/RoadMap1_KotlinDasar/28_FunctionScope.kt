package com.rsb.belajarkotlin.RoadMap1_KotlinDasar

/**
 * function scope adalah ruang lingkup dimana sebuah function bisa di akses
 * saat kita membuat function di dalam file kotlin, maka secara otomatis
 * function tersebut bisa di akses dari file kotlin manapun
 * jika kita ingin membatasi misalnya sebuah function hanya bisa di akses dalam function tertentu, maka kita bisa membuat
 * function di dalam function
 */

fun main() {

    /**
     * scope function function yang hanya bisa di akses dalam satu blok function saja
     * Inner function = function di dalam function
     */
    fun hei(nama: String): String {
        var result = "Halo $nama Selamat Datang di belajar kotlin"
        return result
    }
    println(hei("RORIK SETYA BUDI"))

}