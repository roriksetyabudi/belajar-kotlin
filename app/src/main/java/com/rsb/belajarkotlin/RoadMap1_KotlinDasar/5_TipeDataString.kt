package com.rsb.belajarkotlin.RoadMap1_KotlinDasar

fun main() {
    /**
     * Tipe data string adalah tipe data yang berisikan data kumpulan karakter (text) bisa kosong atau tak terhingga
     * tipe data string di representasikan dengan Tipe Data (String)
     * untuk membuat string kita bisa menggunaan :
     * "(petik dua) untuk membuat text 1 baris
     * """ (petik 2 sebanyak 3 kali) untuk text lebih dari 1 baris
     */

    println("--------- STRING --------- ")

    var firsname: String = "Rorik"
    var lastname: String = "Budi"
    var fullname: String = "Rorik Setya Budi"
    var address: String = """
        |Jl. Panglima Besar Jendral Sudirman,
        |Kecamatan Pacitan,
        |Kabupaten Pacitan,
        |Provinsi Jawa Timur,
        |Negara Indonesia
    """.trimMargin() //trimMargin = digunakan untuk menggabungkan multi line menjadi 1 line saja. di depan karakter trimMargin harus di kasih tanda | secara default untuk mengahapus karakter |
    println("firsname   : " + firsname)
    println("lastname   : " + lastname)
    println("fullname   : " + fullname)
    println("address    : " + address)

    println("--------- PENGGABUNGAN STRING --------- ")
    /**
     * Penggabungan string menggunakan tanda +(plus) diikuti nama var nya
     */
    var fullNameGabung: String = fullname + " " + lastname
    println("fullNameGabung : " + fullNameGabung)

    println("--------- STRING TEMPLATE --------- ")
    /**
     * String template : adalah kemampuan string yang mendukung expresi template
     * Dengan string template, kita bisa mengakses data dari luar text stringnya
     * $ tanda yang digunakan untuk template expresi sederhana, seperti mengakses variable lain
     * ${isi ekspresi} adalah tanda yang digunakan untuk template expresi komplek
     */
    var fullNameStringTemplate: String = "$firsname $lastname"
    var deskripsi: String = "total length $fullNameStringTemplate, length ${fullNameStringTemplate.length}"
    println("fullNameStringTemplate : " + fullNameStringTemplate)
    println("deskripsi : " + deskripsi)


}