package com.rsb.belajarkotlin.RoadMap1_KotlinDasar

/**
 * Varargs = Variable Argument
 * Parameter yang berada di posisi terakhir, memiliki kemampuan dijadikan sebuah varargs
 * varargs artinya datanya bisa menerima lebih dari satu input, atau anggap saja semacam array.
 * apa perbedaanya dengan parameter biasa dengan tipe data array
 * jika parameter tipe array, kita wajib membuat array terlebih dahulu sebelum mengirim ke function
 * jika parameter menggunakan varargs, kita bisa langsung mengirim datanya, jika lebih dari satu, cukup gunakan tanda koma
 */

fun nilaiAkhir(nama: String, vararg nilai: Int) : Int {
    var total = 0
    for (valNilai in nilai) {
        total += valNilai
    }

    var fullNilai = total
    return fullNilai
}

fun main() {
    var totalNilai = nilaiAkhir("RORIK", 10,10,10,10,10)
    println(totalNilai)
}