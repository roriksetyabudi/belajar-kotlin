package com.rsb.belajarkotlin.RoadMap1_KotlinDasar

/**
 * Main function adalah function utama yang di jalankan oleh kotlin
 * untuk menampilkan function main silahkan tekan Ctrl+Spasi (cari main) lalu enter
 * println digunakan untuk menulis plus enter
 * print tidak plus enter
 */

fun main() {
    println("Hello World")
    println("Hello World")
    println("Hello World")

    print("Hello World")
    print("Hello World")
}
