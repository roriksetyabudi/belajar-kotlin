package com.rsb.belajarkotlin.RoadMap1_KotlinDasar

/**
 * Extension Function adalah kemampuan menambahkan function pada tipe data yang sudah ada
 * extension function adalah salah satu fitur yang sangat powerfull, namun harap menggunakan dengan bijak menggunakanya
 * karena, jika terlalu banyak digunakan, akan membuat program sulit dimengerti, karena akan terlihat seperti magic
 * untuk membuat extension function, kita cukup menambahkan tipe data pada nama functionya, lalu diikuti dengan tanda .(titik)
 * untuk mengakses nya di extension function kita bisa menggunakan kata kunci this
 */

fun String.hay(): String {
    var output = "Halo $this Selamat Datang di Belajar Kotlin"
    return output
}
fun Int.jumlah(): Int {
    var jumlahkan = this + 10
    return jumlahkan
}

fun main() {
    var nama: String = "Rorik Setya Budi"
    println(nama.hay())
    var jum: Int = 10
    println(jum.jumlah())
}