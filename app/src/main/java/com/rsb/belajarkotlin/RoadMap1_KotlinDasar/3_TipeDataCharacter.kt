package com.rsb.belajarkotlin.RoadMap1_KotlinDasar

fun main() {
    /**
     * character di kotlin di representasikan dengan tipe data Char
     * untuk membuat characted di di buat menggunakan tanda '(petik satu)
     * character hanya diisi dengan 1 karakter saja tidak lebih
     */

    var char1: Char = 'R'
    var char2: Char = 'O'
    var char3: Char = 'R'
    var char4: Char = 'I'
    var char5: Char = 'K';

    println(char1)
    println(char2)
    println(char3)
    println(char4)
    println(char5)

    print(char1)
    print(char2)
    print(char3)
    print(char4)
    print(char5)

}