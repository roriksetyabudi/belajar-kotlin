package com.rsb.belajarkotlin.RoadMap3_KotlinGeneric.generic02_generic_class

fun main() {
    val myDataString: MyData<String> = MyData("RORIK")
    myDataString.printData()
    val myDataIn: MyData<Int> = MyData<Int>(200)
    myDataIn.printData()
    /**
     * multiple data
     */
    val myDataMultipleData : MyDataMultiple<String, Int> = MyDataMultiple("RORIK", 27)
    myDataMultipleData.printDAtaMultiple()
}