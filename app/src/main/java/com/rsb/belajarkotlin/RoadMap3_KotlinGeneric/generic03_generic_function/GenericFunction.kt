package com.rsb.belajarkotlin.RoadMap3_KotlinGeneric.generic03_generic_function

fun main() {
    val function = Function("RORIK")
    function.hay<String>("DIAN")
    function.hay("BUDI")

    function.hay<Int>(200)
    function.hay(300)
}