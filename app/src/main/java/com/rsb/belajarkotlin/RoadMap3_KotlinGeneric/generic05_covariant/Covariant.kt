package com.rsb.belajarkotlin.RoadMap3_KotlinGeneric.generic05_covariant

class Covariant<out T>(val data: T) {
    fun data(): T{
        return data
    }
}

fun main() {
    val data1 : Covariant<String> = Covariant("RORIK")
    val data2 : Covariant<Any> = data1
    println(data2.data())
    println(data2.data)
}