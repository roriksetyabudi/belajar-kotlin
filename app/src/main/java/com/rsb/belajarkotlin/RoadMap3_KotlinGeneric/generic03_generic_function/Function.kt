package com.rsb.belajarkotlin.RoadMap3_KotlinGeneric.generic03_generic_function

class Function (val nama: String) {
    fun <T> hay(params: T) {
        println("Hallo $params My Name Is ${this.nama}")
    }
}