package com.rsb.belajarkotlin.RoadMap3_KotlinGeneric.generic11_comparable_interface

class Fruit(val name: String, val quantity: Int) : Comparable<Fruit> {
    override fun compareTo(other: Fruit): Int {
        return quantity.compareTo(other.quantity)
    }

}