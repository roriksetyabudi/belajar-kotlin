package com.rsb.belajarkotlin.RoadMap3_KotlinGeneric.generic12_readonly_property_interface

import kotlin.properties.ReadOnlyProperty
import kotlin.reflect.KProperty

class LogReadOnlyProperty(val data: String) : ReadOnlyProperty<Any, String> {
    var counter: Int  = 0
    override fun getValue(thisRef: Any, property: KProperty<*>): String {
        println("Akses Property ${property.name} with value $data")
        counter++
        return data + counter
    }
}
class NameWithLog(param: String) {
    val name: String by LogReadOnlyProperty(param)
}

fun main() {
    val nameWithLog = NameWithLog("RORIK SETYA BUDI")
    println(nameWithLog.name)
    println(nameWithLog.name)
}