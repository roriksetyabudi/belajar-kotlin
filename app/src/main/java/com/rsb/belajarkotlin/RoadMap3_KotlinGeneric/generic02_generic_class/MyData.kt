package com.rsb.belajarkotlin.RoadMap3_KotlinGeneric.generic02_generic_class

class MyData<T>(val firstData: T) {
    fun printData() {
        return println("Data Is $firstData")
    }
    fun getData() : T {
        return  firstData
    }
}