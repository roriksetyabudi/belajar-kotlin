package com.rsb.belajarkotlin.RoadMap3_KotlinGeneric.generic09_star_projection

fun displayLength(array: Array<*>) {
    println("Panjang Array ${array.size}")
}
fun main() {
    val array_int = arrayOf(1,2,3,4,5,6,7,8,9)
    val array_string = arrayOf("RORIK", "SETYA", "BUDI")
    println(displayLength(array_int))
    println(displayLength(array_string))
}