package com.rsb.belajarkotlin.RoadMap3_KotlinGeneric.generic06_contravariant

class Contravariant<in T> {
    fun hay(name: T) {
        return println("Hallo $name")
    }
}

fun main() {
    val data1: Contravariant<Any> = Contravariant()
    val data2: Contravariant<String> = data1
    data2.hay("RORIK SETYA BUDI")
}