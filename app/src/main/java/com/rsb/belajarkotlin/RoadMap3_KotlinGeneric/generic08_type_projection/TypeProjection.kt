package com.rsb.belajarkotlin.RoadMap3_KotlinGeneric.generic08_type_projection

class Container<T>(var data: T)
fun copy(from: Container<out Any>, to: Container<Any>) {
    to.data = from.data
}
fun main() {
    val data1 = Container("RORIK 1")
    val data2 : Container<Any> = Container("RORIK 2")
    copy(data1, data2)
    println(data1.data)
    println(data2.data)
}
