package com.rsb.belajarkotlin.RoadMap3_KotlinGeneric.generic15_generic_extension_function

fun main() {
    val data1 : Data<Int> =  Data(10)
    val data2 : Data<String> =  Data("RORIK SETYA BUDI")
    println(data1.printInt())
    println(data2.printString())
}