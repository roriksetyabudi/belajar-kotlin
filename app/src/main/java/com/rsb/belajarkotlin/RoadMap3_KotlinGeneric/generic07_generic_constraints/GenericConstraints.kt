package com.rsb.belajarkotlin.RoadMap3_KotlinGeneric.generic07_generic_constraints

interface canSayHalo{
    fun sayHalo(name: String)
}

open class Employee

class Manager: Employee()

class VicePresident: Employee(), canSayHalo{
    override fun sayHalo(name: String) {
        println("Hallo Name $name, i'am  vice president")
    }
}

class Company<T>(val employee: T) where T : Employee, T : canSayHalo

fun main() {
    //val data0 = Company(Employee())
    //val data1 = Company(Manager())
    val data2 = Company(VicePresident())
    println(data2.employee.sayHalo("RORIK SETYA BUDI"))

}