package com.rsb.belajarkotlin.RoadMap3_KotlinGeneric.generic13_ReadWriteProperty_interface

import kotlin.properties.Delegates
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

class StringLogReadWriteProperty(var data: String) : ReadWriteProperty<Any, String> {
    override fun getValue(thisRef: Any, property: KProperty<*>): String {
        println("You Get Data ${property.name} with value $data")
        return data
    }

    override fun setValue(thisRef: Any, property: KProperty<*>, value: String) {
        println("you set data ${property.name} from $data to ${value}")
        data = value
    }

}
class Person(param: String) {
    var name: String by StringLogReadWriteProperty(param)
}

fun main() {
    val person = Person("RORIK")

    println(person.name)
    person.name = "SEYA BUDI"
    println(person.name)
}