package com.rsb.belajarkotlin.RoadMap3_KotlinGeneric.generic02_generic_class

class MyDataMultiple<T, U>(val firstData: T, val secondDaata: U) {
    fun printDAtaMultiple() {
       return println("DAta Is $firstData dan $secondDaata")
    }

    fun getSecondData() : U {
        return secondDaata
    }
    fun getData() : T {
        return firstData
    }
}