package com.rsb.belajarkotlin.RoadMap3_KotlinGeneric.generic15_generic_extension_function

class Data<T>(val data: T)
fun Data<String>.printString(){
    val string = this.data
    println("String Value Is $string")
}
fun Data<Int>.printInt(){
    val int = this.data
    println("Int Value IS $int")
}