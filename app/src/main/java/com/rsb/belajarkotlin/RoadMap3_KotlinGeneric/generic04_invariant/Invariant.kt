package com.rsb.belajarkotlin.RoadMap3_KotlinGeneric.generic04_invariant

class Invariant<T>(val data : T)

fun main() {
    val invariantString = Invariant("String")
    println(invariantString.data)
}