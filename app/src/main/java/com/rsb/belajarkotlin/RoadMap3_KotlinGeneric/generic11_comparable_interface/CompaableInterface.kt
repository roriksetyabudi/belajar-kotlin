package com.rsb.belajarkotlin.RoadMap3_KotlinGeneric.generic11_comparable_interface

fun main() {

    val fruit1 = Fruit("Mangga", 10)
    val fruit2 = Fruit("Anggur", 20)
    println(fruit1 > fruit2)
    println(fruit1 < fruit1)
    println(fruit1 >= fruit2)
    println(fruit1 <= fruit2)
}