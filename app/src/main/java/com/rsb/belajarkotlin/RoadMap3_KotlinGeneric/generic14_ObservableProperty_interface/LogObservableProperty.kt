package com.rsb.belajarkotlin.RoadMap3_KotlinGeneric.generic14_ObservableProperty_interface

import kotlin.properties.Delegates
import kotlin.properties.ObservableProperty
import kotlin.reflect.KProperty

class LogObservableProperty<T>(data: T): ObservableProperty<T>(data) {
    override fun beforeChange(property: KProperty<*>, oldValue: T, newValue: T): Boolean {
        println("Before change ${property.name} value from $oldValue to $newValue")
        return true
    }

    override fun afterChange(property: KProperty<*>, oldValue: T, newValue: T) {
        println("After Change ${property.name} value from $oldValue to $newValue")

    }
}

class Car(brand: String) {
    var brand: String by LogObservableProperty(brand)
    var owner: String by Delegates.notNull<String>()
    var deskripsi : String by Delegates.vetoable("") { property, oldValue, newValue ->
        println("Before Change ${property.name} from $oldValue to $newValue")
        true
    }
    var other : String by Delegates.observable("") { property, oldValue, newValue ->
        println("After Change ${property.name} from $oldValue to $newValue")
    }
}

fun main() {
    val car = Car("TOYOTA")
    car.brand = "DAIHATSU"
    println(car.brand)

    car.owner = "RORIK"
    println(car.owner)

    car.deskripsi = "Deskripsi"
    car.other = "other"
    println(car.deskripsi)
    println(car.other)

}