package com.rsb.belajarkotlin.RoadMap4_KotlinCollection.`41_destructuring_declaration_map`

fun main() {
    val map = mapOf("a" to "Rorik", "b" to "Setya", "c" to "Budi")
    for (entry in map) {
        println("${entry.key} : ${entry.value}")
    }
    println("------------------------------------------")
    map.forEach { entry -> println("${entry.key} : ${entry.value}") }
}