package com.rsb.belajarkotlin.RoadMap4_KotlinCollection.`11_mapping`

import com.rsb.belajarkotlin.RoadMap1_KotlinDasar.factorialLoop


fun main() {
    val list1: List<String> = listOf("Rorik", "Setya", "Budi")
    val list2: List<String> = list1.map { item -> item.toUpperCase() }
    val list3: List<String> = list1.map { it.toLowerCase() }
    val list4: List<Int> = list1.map { item -> item.length }
    val list5: List<String> = list1.mapNotNull { value ->
        if(value == "RORIk") {
            value
        } else {
            null
        }

    }
    println(list1)
    println(list2)
    println(list3)
    println(list4)
    println(list5)
    val names = listOf("RORIK", "DANI", "BUDI", "JOKO", "ANDRI")
    val namesGanjil = names.mapIndexedNotNull{ index, name ->
        if(index % 2 == 0) {
            null
        } else {
            name
        }
    }
    println(namesGanjil)
    val numbers = (1..100).toList()
    val numberganjil = numbers.mapNotNull {
        if(it % 2 == 0) {
            null
        } else {
            it
        }
    }
    println(numberganjil)
    for (valueGanjil in numberganjil) {
        println("Nilai Ganjil : $valueGanjil")
    }
    /**
     * Mapping map
     */
    val map1: Map<Int, String> = mapOf(
        1 to "rorik",
        2 to "setya",
        3 to "budi"
    )
    val map2: Map<Int, String> = map1.mapKeys {
        it.key * 10
    }
    val map3: Map<Int, String> = map1.mapValues {
        it.value.toUpperCase()
    }
    println(map1)
    println(map2)
    println(map3)
}
