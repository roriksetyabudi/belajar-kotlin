package com.rsb.belajarkotlin.RoadMap4_KotlinCollection.`25_retrive_by_position`

fun main() {
    val list = listOf("rorik","setya", "budi")

    println(list.first())
    println(list.last())
    println(list.elementAt(1))
    println(list.elementAtOrNull(10))
    println(list.elementAtOrElse(10) { "Kosong" })

}