package com.rsb.belajarkotlin.RoadMap4_KotlinCollection.`02_list`

fun main() {
    println("------------IMMUTABLE LIST------------")
    val list: List<String> = listOf("RORIK", "SETYA", "BUDI")
    println(list[0])
    println(list[1])
    println(list[2])
    println(list.isEmpty())
    println(list.isNotEmpty())
    println(list.indexOf("BUDI"))
    println(list.contains("RORIK"))
    println(list.containsAll(listOf("BUDI","SETYA")))

    /**
     * Mutable List
     */
    println("------------MUTABLE LIST------------")
    val mutableList : MutableList<String> = mutableListOf()
    mutableList.add("RORIKS")
    mutableList.add("SETYAS")
    mutableList.add("BUDIS")
    println(mutableList[0])
    println(mutableList[1])
    println(mutableList[2])

    //menggunakan forr
    for(value in mutableList) {
        println("Value Fornya : $value")
    }

    println(mutableList.isEmpty())
    println(mutableList.isNotEmpty())
}