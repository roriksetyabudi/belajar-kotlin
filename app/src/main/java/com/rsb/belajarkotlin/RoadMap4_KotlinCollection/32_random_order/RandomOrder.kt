package com.rsb.belajarkotlin.RoadMap4_KotlinCollection.`32_random_order`

fun main() {
    val number = (1..20).toList()

    println(number.shuffled())
    println(number.shuffled())
    println(number.shuffled())
    println(number.shuffled())
    println(number.shuffled())
}