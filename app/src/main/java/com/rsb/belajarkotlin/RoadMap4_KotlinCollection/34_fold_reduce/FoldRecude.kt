package com.rsb.belajarkotlin.RoadMap4_KotlinCollection.`34_fold_reduce`

fun main() {
    val number = (1..100).toList().shuffled()
    val max = number.reduce { acc, i ->
        if(acc < i) i
        else acc
    }
    val min = number.reduce { first, second ->
        if(first < second) first
        else second
    }
    val sum = number.fold(0) { acc, i ->
        acc + i
    }
    println(max)
    println(min)
    println(sum)

    val name = listOf("RORIK", "SETYA", "BUDI")
    val count = name.fold(0) { first, second ->
        first+1
    }
    println(count)
}