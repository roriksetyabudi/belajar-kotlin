package com.rsb.belajarkotlin.RoadMap4_KotlinCollection.`06_collection`

/**
 * Immutable Collection
 */
fun <T> displayCollection(collection: Collection<T>) {
    for (item in collection) {
        println(item)
    }
}

/**
 * Mutable Collection
 */
fun <T> displayMutableCollection(collection: MutableCollection<T>) {
    for(item in collection) {
        println(item)
    }
}

fun main() {
    println("------------IMMUTABLE COLLECTION------------")

    displayCollection(listOf("RORIK", "SETYA", "BUDI"))
    displayCollection(setOf("JOKO", "SETYO", "NUGROHO"))
    displayCollection(mapOf("a" to "rorik").entries)
    displayCollection(mapOf("b" to "setya").entries)
    displayCollection(mapOf("c" to "jeki").entries)

    println("------------MUTABLE COLLECTION------------")
    displayMutableCollection(mutableListOf("RORIK", "SETYA", "BUDI"))
    displayMutableCollection(mutableSetOf("JOKO", "SETYO", "NUGROHO"))
    displayMutableCollection(mutableMapOf("a" to "RORIK").entries)
    displayMutableCollection(mutableMapOf("b" to "SETYA").entries)


}