package com.rsb.belajarkotlin.RoadMap4_KotlinCollection.`20_grouping`

fun main() {
    val list1 = listOf("aa","a","bb","c","b","c","d","d","e", "ee","a")
    val map: Map<String, List<String>> = list1.groupBy { it }
    val map2: Map<Int, List<String>> = list1.groupBy{ it.length }
    val group: Grouping<String, String> = list1.groupingBy { it }

    println(map)
    println(map2)
    println(group)
}