package com.rsb.belajarkotlin.RoadMap4_KotlinCollection.`19_plus_minus_operators`

fun main() {
    val list1 = listOf("Rorik", "Setya","Budi")
    val list2 = list1 + "Joko"
    val list3 = list1 + listOf("Programmer", "RSUD")
    val list4 = list1 - "Budi"
    val list5 = list1 - listOf("Setya","Budi")

    val map1 = mapOf("a" to "rorik", "b" to "Setya", "c" to "Budi")
    val map2 = map1 + ("c" to "BudiGunadi")
    val map3 = map1 - "a"

    println(list1)
    println(list2)
    println(list3)
    println(list4)
    println(list5)

    println(map1)
    println(map2)
    println(map3)
}