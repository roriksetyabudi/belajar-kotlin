package com.rsb.belajarkotlin.RoadMap4_KotlinCollection.`27_retrive_by_random`

fun main() {
    val range = (1..100).toList()

    println(range.random())
    println(range.random())
    println(range.random())
    println(range.random())
    println(range.random())
}