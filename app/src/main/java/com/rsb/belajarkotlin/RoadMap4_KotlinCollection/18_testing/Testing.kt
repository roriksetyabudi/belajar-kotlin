package com.rsb.belajarkotlin.RoadMap4_KotlinCollection.`18_testing`

fun main() {
    val list = listOf("Rorik", "Setya", "Budi")
    println(list.any{ it.length > 4 })
    println(list.none{ it.length > 4 })
    println(list.all { it.length > 3 })
    println(list.any())
    println(list.none())
}
