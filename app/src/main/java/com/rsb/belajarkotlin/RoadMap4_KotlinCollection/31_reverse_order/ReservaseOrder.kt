package com.rsb.belajarkotlin.RoadMap4_KotlinCollection.`31_reverse_order`

fun main() {
    val list1 = mutableListOf("rorik","setya","budi")
    val listReserve = list1.reversed()
    val listAsReserve = list1.asReversed()
    println(listReserve)
    println(listAsReserve)
    list1.add("Programmer")
    list1.add("Membaca")
    println(listReserve)
    println(listAsReserve)
}