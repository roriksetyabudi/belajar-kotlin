package com.rsb.belajarkotlin.RoadMap4_KotlinCollection.`12_zipping`

fun main() {
    val list1 = listOf("Rorik", "Setya", "Budi")
    val list2 = listOf("Budi", "Adi", "Gunawan")

    println("------------ ZIP------------")
    val list3: List<Pair<String, String>> = list1.zip(list2)
    val list4: List<String> = list1.zip(list2) { item1, item2 ->
        "$item1 $item2"
    }
    println(list3)
    println(list4)
    println("------------ UNZIP STRING TO STRING------------")
    val listUnzip: List<Pair<String, String>> = listOf(
        "RORIK" to "DIAN",
        "SETYA" to "DINO",
        "BUDI" to "ADI"
    )
    val pair: Pair<List<String>, List<String>> = listUnzip.unzip()
    println(pair)

    println("------------ UNZIP STRING TO INT------------")
    val listUnzipInt: List<Pair<String, Int>> = listOf(
        "RORIK" to 20,
        "SETYA" to 21,
        "BUDI" to 22
    )
    val pair2: Pair<List<String>, List<Int>> = listUnzipInt.unzip()
    println(pair2)
    println("------------ DESTRUCTURING------------")
    val (val1, val2) = listUnzipInt.unzip()
    println(val1)
    println(val2)

}