package com.rsb.belajarkotlin.RoadMap4_KotlinCollection.`39_sequence`

fun main() {
    /**
     * Bukan Sequance
     */
    val word = "the quick brown fox fumps over the laxy dog".split(" ")
    val result = word
        .filter {
            println("filter : $it")
            it.length > 3
        }
        .map {
            println("map : $it")
            it.toUpperCase()
        }
        .take(4)
    println(result)

    println("-------------------- SEQUENCE-------------------- ")
    val sequence = word.asSequence()

    val resultSequence = sequence
        .filter {
            println("filter : $it")
            it.length > 3
        }
        .map {
            println("map : $it")
            it.toUpperCase()
        }
        .take(4)
    println(resultSequence.toList())
}