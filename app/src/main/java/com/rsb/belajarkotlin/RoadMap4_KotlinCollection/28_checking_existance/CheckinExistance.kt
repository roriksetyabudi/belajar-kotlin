package com.rsb.belajarkotlin.RoadMap4_KotlinCollection.`28_checking_existance`

fun main() {
    val range = (1..20).toList()

    println(range.contains(5))
    println(range.containsAll(listOf(1,8,6,2,4)))
    println(range.isEmpty())
    println(range.isNotEmpty())
}