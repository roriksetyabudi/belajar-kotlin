package com.rsb.belajarkotlin.RoadMap4_KotlinCollection.`22_take_dan_drop`

fun main() {
    var char = ('a'..'z').toList()

    println(char.take(3)) //[a,b,c]
    println(char.takeWhile { it < 'f' }) //[a,b,c,d,e]
    println(char.takeLast(2)) //[y, z]
    println(char.takeLastWhile { it > 'w' }) //[x,y,z]

    println(char.drop(23)) //[x,y,z]
    println(char.dropLast(23)) //[a,b,c]
    println(char.dropWhile { it < 'x' }) //[x,y,z]
    println(char.dropLastWhile { it > 'c' }) //[a,b,c]
    for (valChars in char) {
        println(valChars)
    }
}