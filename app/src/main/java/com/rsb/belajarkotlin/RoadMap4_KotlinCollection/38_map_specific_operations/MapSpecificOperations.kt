package com.rsb.belajarkotlin.RoadMap4_KotlinCollection.`38_map_specific_operations`

fun main() {
    val map = mapOf("a" to "RORIK", "b" to "SETYA", "c" to "budi")

    println(map.get("a"))
    //println(map.getValue("AA")) //error
    println(map.getValue("b"))
    println(map.getOrElse("x") { "Tidak Ada" })
    println(map.filter { entry -> entry.value.length > 4 })
    println(map.filterKeys { it != "a" })
    println(map.filterValues { it.length > 4 })
}