package com.rsb.belajarkotlin.RoadMap4_KotlinCollection.`30_custom_order`

data class Fruit(val name: String, val quantity: Int)

fun main() {
    val fruit = listOf(
        Fruit("Apel", 10),
        Fruit("Jambu", 11),
        Fruit("Mangga", 50),
        Fruit("Srikaya", 30),
    )
    println(fruit.sortedBy { it.quantity })
    println(fruit.sortedByDescending { it.quantity })
    println(fruit.sortedWith(compareBy { it.quantity }))
    println(fruit.sortedWith(compareByDescending { it.quantity }))
    println(fruit.sortedWith(Comparator { a, b ->
        a.quantity.compareTo(b.quantity)
    }))
}