package com.rsb.belajarkotlin.RoadMap4_KotlinCollection.`05_map`

fun main() {
    println("------------IMMUTABLE MAP------------")
    val map: Map<String, String> = mapOf(
        Pair("x" , "Jono"),
        "a" to "RORIK",
        "b" to "SETYA",
        "c" to "BUDI",
        "d" to "JOKO",
        "e" to "SETYO",
        "f" to "NUGROHO"
    )
    println(map.size)
    println(map["a"])
    println(map["b"])
    println(map["c"])
    println(map["d"])
    println(map["e"])
    println(map["f"])
    for(valueMap in map) {
        println("Key : ${valueMap.key} value : ${valueMap.value}")
    }
    println("------------MUTABLE MAP------------")

    val mutableMap: MutableMap<String, String> = mutableMapOf()
    mutableMap["a"] = "RORIK MUTABLE"
    mutableMap["b"] = "SETYA MUTABLE"
    mutableMap["c"] = "BUDI MUTABLE"
    mutableMap["d"] = "JOKO MUTABLE"
    mutableMap["e"] = "SETYO MUTABLE"
    mutableMap["f"] = "NUGROHO MUTABLE"
    mutableMap.put("g", "MURDIANTO")
    mutableMap.put("h", "RIANTO")

    println(mutableMap["a"])
    println(mutableMap["b"])
    println(mutableMap["c"])
    println(mutableMap["d"])
    println(mutableMap["e"])
    println(mutableMap["f"])
    println(mutableMap["g"])
    println(mutableMap.get("h"))

    mutableMap.remove("b") //jika ingin remove

    for(valueMutable in mutableMap) {
        println("Key Mutable : ${valueMutable.key} , value mutable : ${valueMutable.value}")
    }


}
