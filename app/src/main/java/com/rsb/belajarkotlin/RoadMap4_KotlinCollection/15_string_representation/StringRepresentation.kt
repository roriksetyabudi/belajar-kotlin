package com.rsb.belajarkotlin.RoadMap4_KotlinCollection.`15_string_representation`

fun main() {
    val name = listOf("rorik", "setya", "budi")
    println(name.joinToString ("|","|","|"))
    println(name.joinToString(" ", "|", "|'"){ string -> "item $string" })

    val appendable = StringBuilder()
    name.joinTo(appendable, ",", "|", "|") { it.toUpperCase() }
    name.joinTo(appendable, ">", "|", "|") { it.toUpperCase() }
    println(appendable.toString())

    val numbers: List<Int> = (1..20).toList()
    println(numbers.joinToString() )
}