package com.rsb.belajarkotlin.RoadMap4_KotlinCollection.`14_flattening`

import java.util.stream.Stream

/**
 * Flat map
 */
class Member(val name: String, val hobi: List<String>)

fun main() {
    val list1: List<List<String>> = listOf(
        listOf("rorik", "setya", "budi"),
        listOf("eko", "joko", "budi"),
        listOf("jamil", "ganang", "danang")
    )
    val list2: List<String> = list1.flatten()
    println(list2)
    /**
     * Flat Map
     */
    val members: List<Member> = listOf(
        Member("Rorik", listOf("Membaca", "Belajar", "Coding")),
        Member("Setya", listOf("Coding", "Gamming", "Traveling")),
    )
    val makeHobi: List<String> = members.flatMap { it.hobi }
    println(makeHobi)
}