package com.rsb.belajarkotlin.RoadMap4_KotlinCollection.`35_grouping_interface`

fun main() {
    val list1 = listOf("a","b","c","d","e","a","b","c","d","e","a")
    val grouping: Grouping<String, String> = list1.groupingBy { it }
    println(grouping.eachCount())
    println(grouping.fold("") { accumulator, element -> accumulator + element })
    println(grouping.reduce { key, accumulator, element -> accumulator + element })
    println(grouping.aggregate { key, accumulator: String?, element, first ->
        if(first) element
        else accumulator + element
    })
}