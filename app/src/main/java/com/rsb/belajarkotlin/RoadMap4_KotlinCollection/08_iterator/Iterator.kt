package com.rsb.belajarkotlin.RoadMap4_KotlinCollection.`08_iterator`

fun <T> displayListIterator(listIterator: ListIterator<T>) {
    println("---------MAJU---------")
    while (listIterator.hasNext()) {
        println(listIterator.next())
    }
    println("---------MUNDUR---------")
    while (listIterator.hasPrevious()) {
        println(listIterator.previous())
    }
}
fun <T> displayMutableIterator(mutableListIterator: MutableListIterator<T>) {
    while (mutableListIterator.hasNext()) {
        println(mutableListIterator.next())
    }
}
fun removeOddNumber(mutableListIterator: MutableListIterator<Int>) {
    while (mutableListIterator.hasNext()) {
        val item = mutableListIterator.next()
        if(item % 2 == 1) {
            mutableListIterator.remove()
        }
    }
}
fun main() {
    println("------------MUTABLE ITERATOR------------")
    displayListIterator(listOf("RORIK", "SETYA", "BUDI").listIterator())
    displayListIterator(listOf("JOKO", "PUTRO", "UTOMO").listIterator())

    println("------------IMMUTABLE ITERATOR------------")
    val mutablelist = mutableListOf(1,2,3,4,5,6,7,8,9,10)
    removeOddNumber(mutablelist.listIterator())
    displayMutableIterator(mutablelist.listIterator())


}