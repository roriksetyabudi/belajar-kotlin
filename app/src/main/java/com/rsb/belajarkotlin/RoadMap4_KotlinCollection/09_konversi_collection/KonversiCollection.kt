package com.rsb.belajarkotlin.RoadMap4_KotlinCollection.`09_konversi_collection`

fun main() {
    val array = arrayOf(11,14,13,15,19,18,16,20,12,17)
    val range = 1..10
    //range to list
    val list = range.toList()
    //array to mutablelist
    val mutableList = array.toMutableList()
    //list to set
    val set = list.toSet()
    //list to mutableSet
    val mutableSet = list.toMutableSet()
    //list to sorted set
    val sortedSet = list.toSortedSet()

    println(list)
    println(mutableList)
    println(set)
    println(mutableSet)
    println(sortedSet)



}