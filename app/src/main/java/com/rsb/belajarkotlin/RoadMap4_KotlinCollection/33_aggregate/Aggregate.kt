package com.rsb.belajarkotlin.RoadMap4_KotlinCollection.`33_aggregate`

fun main() {
    val number = (1..100).toList()
    println(number.max())
    println(number.min())
    println(number.average())
    println(number.sum())
    println(number.count())
    println(number.sumBy { it / 2 })

}