package com.rsb.belajarkotlin.RoadMap4_KotlinCollection.`07_iterable`

/**
 * Immutable
 */
fun <T> displayIterable(iterable: Iterable<T>) {
    val iterator = iterable.iterator()
    while (iterator.hasNext()) {
        println(iterator.next())
    }
}

/**
 * Muttable
 */
fun <T> displayMutableIterable(iterable: MutableIterable<T>) {
    val iterator = iterable.iterator()
    while (iterator.hasNext()) {
        println(iterator.next())
    }
}
fun main() {
    displayIterable(listOf("RORIK", "SETYA", "BUDI"))
    displayIterable(setOf("JOKO", "SETYO", "NUGROHO"))
    displayIterable(mapOf("RINO" to "JOKO").entries)

    displayMutableIterable(mutableListOf("BUDI", "SETYA", "RORIK"))
    displayMutableIterable(mutableSetOf("JOKO", "RINO", "IBRAHIM"))

}