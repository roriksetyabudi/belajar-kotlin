package com.rsb.belajarkotlin.RoadMap4_KotlinCollection.`13_association`

fun main() {
    val list1 = listOf("Rorik", "Setya", "Budi")
    val map1: Map<String, Int> = list1.associate { Pair(it, it.length) }
    val map2: Map<String, Int> = list1.associateWith { it.length }
    val map3: Map<Int, String> = list1.associateBy { it.length }

    println(map1)
    println(map2)
    println(map3)
}