package com.rsb.belajarkotlin.RoadMap4_KotlinCollection.`03_set`

import com.rsb.belajarkotlin.RoadMap2_KotlinOOP.oop17_type_check_casts.printObjectUnsafeCasts

fun main() {
    println("------------IMMUTABLE SET------------")
    val set: Set<Person> = setOf(
        Person("RORIK"),
        Person("RORIK"),
        Person("SETYA"),
        Person("BUDI"),
        Person("JOKO")
    )
    println(set.size)
    println(set.contains(Person("BUDI")))
    println(set.isEmpty())
    println(set.isNotEmpty())
    println(set.containsAll(setOf("RORIK", "SETYA")))

    for(value in set) {
        println("Person $value")
    }
    println("------------MUTABLE SET------------")
    /**
     * Mutable Set
     */
    val mutableSet: MutableSet<Person> = mutableSetOf()
    mutableSet.add(Person("RORIK ADD"))
    mutableSet.add(Person("SETYA ADD"))
    mutableSet.add(Person("BUDI ADD"))
    mutableSet.add(Person("JOKO ADD"))
    mutableSet.add(Person("RORIK ADD"))

    println(mutableSet.size)
    println(mutableSet.contains(Person("RORIK")))
    for(valueMutable in mutableSet) {
        println("Person Mutable $valueMutable")
    }
}