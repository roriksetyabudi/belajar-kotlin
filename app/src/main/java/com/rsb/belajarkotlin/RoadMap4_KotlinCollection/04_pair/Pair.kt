package com.rsb.belajarkotlin.RoadMap4_KotlinCollection.`04_pair`

fun main() {
    val pair1: Pair<String, String> = Pair("RORIK","SETYA")
    println(pair1.first)
    println(pair1.second)

    val pair2: Pair<String, String> = "SETYA" to "BUDI"
    println(pair2.first)
    println(pair2.second)


}