package com.rsb.belajarkotlin.RoadMap4_KotlinCollection.`10_collection_operations`

fun main() {
    listOf("RORIK", "SETYA", "BUDI").forEach {
        value -> println(value)
    }
    listOf("JOKO", "SETYO", "NUGROHO").forEachIndexed { index, value ->
        println("$index = $value")
    }
    mutableListOf("RION", "BUDI", "SETIAWAN").forEachIndexed{ index, value ->
        println("$index = $value")
    }
}