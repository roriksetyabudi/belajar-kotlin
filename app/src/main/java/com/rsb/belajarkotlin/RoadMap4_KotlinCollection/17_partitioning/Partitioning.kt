package com.rsb.belajarkotlin.RoadMap4_KotlinCollection.`17_partitioning`

fun main() {
    val list1 = listOf("Rorik", "Setya", "Budi")
    val (listMatch, listNotMatch) = list1.partition { it.length > 4 }
    println(list1)
    println(listMatch)
    println(listNotMatch)
}