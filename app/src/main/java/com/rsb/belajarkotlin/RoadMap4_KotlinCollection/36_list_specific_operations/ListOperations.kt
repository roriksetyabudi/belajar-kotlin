package com.rsb.belajarkotlin.RoadMap4_KotlinCollection.`36_list_specific_operations`

fun main() {
    val list = listOf("RORIK", "SETYA", "BUDI")

    println(list.get(1))
    println(list.getOrNull(10))
    println(list.getOrElse(10){ index -> "Tidak Ada" })
    println(list.subList(0, 2))

    val sortedList = list.sorted()
    println(sortedList)
    println(sortedList.binarySearch("RORIK"))
    println(sortedList.binarySearch("SETYA"))
    println(sortedList.binarySearch("BUDI"))

    val number = listOf(1,1,2,2,3,3,4,4,5,5,6,6,7,7,8,8,9,9)
    println(number.indexOf(2))
    println(number.lastIndexOf(2))
    println(number.indexOfFirst { it > 3 })
    println(number.indexOfLast { it > 3 })
}