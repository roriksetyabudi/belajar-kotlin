package com.rsb.belajarkotlin.RoadMap4_KotlinCollection.`29_ordering`

fun main() {
    val number = listOf(8,5,4,7,8,1,2,6,9,7,10,11,15,16,18,21,20)

    println(number)
    println(number.sorted())
    println(number.sortedDescending())
}